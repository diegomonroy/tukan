<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
Schema::table('documents', function (Blueprint $table) {
$table->integer('location_country_1_id')->nullable();
$table->integer('location_department_1_id')->nullable();
$table->integer('location_township_1_id')->nullable();
$table->integer('location_country_2_id')->nullable();
$table->integer('location_department_2_id')->nullable();
$table->integer('location_township_2_id')->nullable();
$table->integer('location_country_3_id')->nullable();
$table->integer('location_department_3_id')->nullable();
$table->integer('location_township_3_id')->nullable();
$table->integer('location_country_4_id')->nullable();
$table->integer('location_department_4_id')->nullable();
$table->integer('location_township_4_id')->nullable();
$table->integer('location_country_5_id')->nullable();
$table->integer('location_department_5_id')->nullable();
$table->integer('location_township_5_id')->nullable();
$table->text('tourism_tags')->nullable();
$table->text('topic_tags')->nullable();
$table->string('source', 255)->nullable();
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
Schema::table('documents', function (Blueprint $table) {
$table->dropColumn([
'location_country_1_id',
'location_department_1_id',
'location_township_1_id',
'location_country_2_id',
'location_department_2_id',
'location_township_2_id',
'location_country_3_id',
'location_department_3_id',
'location_township_3_id',
'location_country_4_id',
'location_department_4_id',
'location_township_4_id',
'location_country_5_id',
'location_department_5_id',
'location_township_5_id',
'tourism_tags',
'topic_tags',
'source',
]);
});
    }
}
