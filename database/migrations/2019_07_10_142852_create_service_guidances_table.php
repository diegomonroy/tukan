<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceGuidancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('service_guidances', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('characterization_id');
			$table->string('name', 255);
			$table->text('type_of_guidance');
			$table->text('description')->nullable();
			$table->text('specifications')->nullable();
			$table->integer('minimum_of_travelers')->nullable();
			$table->integer('maximum_of_travelers')->nullable();
			$table->text('share_some_photos')->nullable();
			$table->text('share_some_videos')->nullable();
			$table->string('location', 255)->nullable();
			$table->enum('teaching_material', ['yes', 'no'])->nullable();
			$table->text('teaching_material_file')->nullable();
			$table->enum('accreditation', ['yes', 'no'])->nullable();
			$table->integer('professional_card')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('service_guidances');
    }
}
