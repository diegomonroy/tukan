<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceAccommodationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('service_accommodations', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('characterization_id');
			$table->string('name', 255);
			$table->integer('type_of_accommodation_id');
			$table->text('description')->nullable();
			$table->integer('minimum_of_travelers')->nullable();
			$table->integer('maximum_of_travelers')->nullable();
			$table->text('share_some_photos')->nullable();
			$table->text('share_some_videos')->nullable();
			$table->string('location', 255)->nullable();
			$table->enum('air_conditioner', ['yes', 'no'])->nullable();
			$table->enum('health_services', ['yes', 'no'])->nullable();
			$table->enum('drinking_water', ['yes', 'no'])->nullable();
			$table->enum('electric_power', ['yes', 'no'])->nullable();
			$table->enum('telephony', ['yes', 'no'])->nullable();
			$table->enum('internet', ['yes', 'no'])->nullable();
			$table->integer('accommodation_for_id')->nullable();
			$table->integer('people_in_total')->nullable();
			$table->integer('simple_accommodation')->nullable();
			$table->integer('double_accommodation')->nullable();
			$table->integer('shared_accommodation')->nullable();
			$table->integer('other_types_of_accommodation')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
		Schema::create('select_type_of_accommodations', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->unique();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
		Schema::create('select_accommodation_fors', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->unique();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('service_accommodations');
		Schema::dropIfExists('select_type_of_accommodations');
		Schema::dropIfExists('select_accommodation_fors');
    }
}
