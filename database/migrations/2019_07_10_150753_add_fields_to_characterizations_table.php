<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToCharacterizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('characterizations', function (Blueprint $table) {
			$table->enum('rut', ['yes', 'no'])->nullable()->after('volunteer');
			$table->bigInteger('rut_number')->nullable()->after('rut');
			$table->text('rut_file')->nullable()->after('rut_number');
			$table->enum('rnt', ['yes', 'no'])->nullable()->after('rut_file');
			$table->bigInteger('rnt_number')->nullable()->after('rnt');
			$table->text('rnt_file')->nullable()->after('rnt_number');
			$table->integer('payment_method_id')->nullable()->after('rnt_file');
			$table->bigInteger('bank_number')->nullable()->after('payment_method_id');
			$table->bigInteger('efecty_number')->nullable()->after('bank_number');
			$table->string('contact_name', 255)->nullable()->after('efecty_number');
			$table->bigInteger('contact_document')->nullable()->after('contact_name');
			$table->bigInteger('contact_phone')->nullable()->after('contact_document');
		});
		Schema::create('select_payment_methods', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->unique();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('characterizations', function (Blueprint $table) {
			$table->dropColumn([
				'rut',
				'rut_number',
				'rut_file',
				'rnt',
				'rnt_number',
				'rnt_file',
				'payment_method_id',
				'bank_number',
				'efecty_number',
				'contact_name',
				'contact_document',
				'contact_phone'
			]);
		});
		Schema::dropIfExists('select_payment_methods');
    }
}
