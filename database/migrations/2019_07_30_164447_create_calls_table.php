<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('calls', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name', 255);
			$table->enum('type', ['internal', 'external'])->nullable();
			$table->string('objective', 255)->nullable();
			$table->text('features')->nullable();
			$table->text('what_do_you_need')->nullable();
			$table->date('deadline')->nullable();
			$table->string('link', 255)->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('calls');
    }
}
