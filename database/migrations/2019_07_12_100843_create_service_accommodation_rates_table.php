<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceAccommodationRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('service_accommodation_rates', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('service_accommodation_id');
			$table->integer('price');
			$table->integer('price_other')->nullable();
			$table->date('since_when');
			$table->date('even_when');
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('service_accommodation_rates');
    }
}
