<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceFeedingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('service_feedings', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('characterization_id');
			$table->string('name', 255);
			$table->integer('type_of_feeding_id');
			$table->text('description')->nullable();
			$table->text('specifications')->nullable();
			$table->integer('feeding_preferential_id')->nullable();
			$table->text('share_some_photos')->nullable();
			$table->text('share_some_videos')->nullable();
			$table->string('location', 255)->nullable();
			$table->enum('food_handling', ['yes', 'no'])->nullable();
			$table->enum('drinking_water', ['yes', 'no'])->nullable();
			$table->enum('electric_power', ['yes', 'no'])->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
		Schema::create('select_type_of_feedings', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->unique();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
		Schema::create('select_feeding_preferentials', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->unique();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('service_feedings');
		Schema::dropIfExists('select_type_of_feedings');
		Schema::dropIfExists('select_feeding_preferentials');
    }
}
