<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharacterizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('characterizations', function (Blueprint $table) {
			$table->increments('id');
			$table->string('email', 255)->unique();
			$table->string('first_name', 255);
			$table->string('last_name', 255);
			$table->bigInteger('phone')->nullable();
			$table->text('organization_name')->nullable();
			$table->text('organization_description')->nullable();
			$table->string('logo', 255)->nullable();
			$table->text('share_some_photos')->nullable();
			$table->integer('in_what_state_id')->nullable();
			$table->date('since_when')->nullable();
			$table->integer('how_big_is_id')->nullable();
			$table->integer('location_country_id')->nullable();
			$table->integer('location_department_id')->nullable();
			$table->integer('location_township_id')->nullable();
			$table->string('organization_address', 255)->nullable();
			$table->string('website', 255)->nullable();
			$table->string('facebook', 255)->nullable();
			$table->string('instagram', 255)->nullable();
			$table->string('twitter', 255)->nullable();
			$table->string('youtube', 255)->nullable();
			$table->string('other_social_networks', 255)->nullable();
			$table->integer('type_of_tourism_id')->nullable();
			$table->integer('ethnic_approach_id')->nullable();
			$table->integer('age_approach_id')->nullable();
			$table->enum('reduced_mobility', ['yes', 'no'])->nullable();
			$table->string('zone_id', 255)->nullable();
			$table->enum('volunteer', ['yes', 'no'])->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
		Schema::create('select_in_what_states', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->unique();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
		Schema::create('select_how_big_iss', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->unique();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
		Schema::create('location_countries', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->unique();
			$table->string('code');
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
		Schema::create('location_departments', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('country_id')->unsigned();
			$table->string('name')->unique();
			$table->string('code');
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
		Schema::create('location_townships', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('department_id')->unsigned();
			$table->string('name')->unique();
			$table->string('code');
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
		Schema::create('location_villages', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('township_id')->unsigned();
			$table->string('name')->unique();
			$table->string('code');
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
		Schema::create('select_type_of_tourisms', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->unique();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
		Schema::create('select_ethnic_approaches', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->unique();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
		Schema::create('select_age_approaches', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->unique();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
		Schema::create('select_zones', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->unique();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('characterizations');
		Schema::dropIfExists('select_in_what_states');
		Schema::dropIfExists('select_how_big_iss');
		Schema::dropIfExists('location_countries');
		Schema::dropIfExists('location_departments');
		Schema::dropIfExists('location_townships');
		Schema::dropIfExists('location_villages');
		Schema::dropIfExists('select_type_of_tourisms');
		Schema::dropIfExists('select_ethnic_approaches');
		Schema::dropIfExists('select_age_approaches');
		Schema::dropIfExists('select_zones');
    }
}
