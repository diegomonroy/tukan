<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceTransportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('service_transports', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('characterization_id');
			$table->string('name', 255);
			$table->integer('type_of_transport_id');
			$table->text('description')->nullable();
			$table->text('specifications')->nullable();
			$table->integer('minimum_of_travelers')->nullable();
			$table->integer('maximum_of_travelers')->nullable();
			$table->text('share_some_photos')->nullable();
			$table->text('share_some_videos')->nullable();
			$table->string('location', 255)->nullable();
			$table->enum('security_devices', ['yes', 'no'])->nullable();
			$table->integer('relevant_documentation_id')->nullable();
			$table->integer('accident_insurance_id')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
		Schema::create('select_type_of_transports', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->unique();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
		Schema::create('select_relevant_documentations', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->unique();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
		Schema::create('select_accident_insurances', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->unique();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('service_transports');
		Schema::dropIfExists('select_type_of_transports');
		Schema::dropIfExists('select_relevant_documentations');
		Schema::dropIfExists('select_accident_insurances');
    }
}
