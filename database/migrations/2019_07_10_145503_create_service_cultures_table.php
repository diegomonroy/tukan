<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceCulturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('service_cultures', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('characterization_id');
			$table->string('name', 255);
			$table->text('type_of_culture');
			$table->text('description')->nullable();
			$table->text('specifications')->nullable();
			$table->text('share_some_photos')->nullable();
			$table->text('share_some_videos')->nullable();
			$table->string('location', 255)->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('service_cultures');
    }
}
