<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * This add the routes for the API's
 */
Route::post('api/department', 'Api\LocationController@department'); // Department
Route::post('api/department/{id}', 'Api\LocationController@department_show'); // Show Department by id
Route::post('api/township', 'Api\LocationController@township'); // Township
Route::post('api/township/{id}', 'Api\LocationController@township_show'); // Show Township by id
Route::post('api/village', 'Api\LocationController@village'); // Village
Route::post('api/village/{id}', 'Api\LocationController@village_show'); // Show Village by id
Route::apiResource('accommodation_rates', 'Api\Service_accommodation_rateController');

/*
 * This add the routes for the Site
 */
Route::get('/{any?}', 'SinglePageApplicationController@index')->where('any', '^(?!api\/)[\/\w\.-]*'); // Show Index page