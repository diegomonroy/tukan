<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    CRUD::resource('select_in_what_state', 'Select_in_what_stateCrudController');
    CRUD::resource('select_how_big_is', 'Select_how_big_isCrudController');
    CRUD::resource('select_type_of_tourism', 'Select_type_of_tourismCrudController');
    CRUD::resource('select_ethnic_approach', 'Select_ethnic_approachCrudController');
    CRUD::resource('select_age_approach', 'Select_age_approachCrudController');
    CRUD::resource('select_zone', 'Select_zoneCrudController');
    CRUD::resource('location_country', 'Location_countryCrudController');
    CRUD::resource('location_department', 'Location_departmentCrudController');
    CRUD::resource('location_township', 'Location_townshipCrudController');
    CRUD::resource('location_village', 'Location_villageCrudController');
    CRUD::resource('characterization', 'CharacterizationCrudController');
    CRUD::resource('select_payment_method', 'Select_payment_methodCrudController');
    CRUD::resource('select_type_of_accommodation', 'Select_type_of_accommodationCrudController');
    CRUD::resource('select_accommodation_for', 'Select_accommodation_forCrudController');
    CRUD::resource('select_type_of_transport', 'Select_type_of_transportCrudController');
    CRUD::resource('select_relevant_documentation', 'Select_relevant_documentationCrudController');
    CRUD::resource('select_accident_insurance', 'Select_accident_insuranceCrudController');
    CRUD::resource('select_type_of_feeding', 'Select_type_of_feedingCrudController');
    CRUD::resource('select_feeding_preferential', 'Select_feeding_preferentialCrudController');
    CRUD::resource('service_accommodation', 'Service_accommodationCrudController');
    CRUD::resource('service_transport', 'Service_transportCrudController');
    CRUD::resource('service_guidance', 'Service_guidanceCrudController');
    CRUD::resource('service_feeding', 'Service_feedingCrudController');
    CRUD::resource('service_culture', 'Service_cultureCrudController');
    CRUD::resource('call', 'CallCrudController');
    CRUD::resource('document', 'DocumentCrudController');
}); // this should be the absolute last line of this file