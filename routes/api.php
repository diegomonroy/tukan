<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
 * This add the routes for the API's
 */
Route::prefix('auth')->group(function () {
	Route::post('register', 'AuthController@register'); // Send Register
	Route::post('login', 'AuthController@login'); // Send Login
	Route::get('refresh', 'AuthController@refresh'); // Do Refresh
	Route::group(['middleware' => 'auth:api'], function () {
		Route::get('user', 'AuthController@user'); // Show User page
		Route::post('logout', 'AuthController@logout'); // Do Logout
	});
});

Route::group(['middleware' => 'auth:api'], function () {
	// Users
	Route::get('users', 'UserController@index')->middleware('isSuperAdministrator'); // Show Index page
	Route::get('users/{id}', 'UserController@show')->middleware('isSuperAdministratorOrSelf'); // Show Show page
});

Route::prefix('v1')->group(function () {
	Route::prefix('auth')->group(function () {
		Route::post('reset-password', 'AuthController@sendPasswordResetLink'); // Send Password Reset Link
		Route::post('reset/password', 'AuthController@callResetPassword'); // Handle Reset Password
	});
});

// Characterization CRUD
Route::post('/characterization/step1', 'CharacterizationController@store');
Route::get('/characterization/step2/{id}', 'CharacterizationController@edit');
// Accommodation CRUD
Route::get('/accommodation', 'Service_accommodationController@index');
Route::post('/accommodation/create', 'Service_accommodationController@store');
Route::get('/accommodation/edit/{id}', 'Service_accommodationController@edit');
Route::post('/accommodation/update/{id}', 'Service_accommodationController@update');
Route::delete('/accommodation/delete/{id}', 'Service_accommodationController@delete');
// Transport CRUD
Route::get('/transport', 'Service_transportController@index');
Route::post('/transport/create', 'Service_transportController@store');
Route::get('/transport/edit/{id}', 'Service_transportController@edit');
Route::post('/transport/update/{id}', 'Service_transportController@update');
Route::delete('/transport/delete/{id}', 'Service_transportController@delete');
// Guidance CRUD
Route::get('/guidance', 'Service_guidanceController@index');
Route::post('/guidance/create', 'Service_guidanceController@store');
Route::get('/guidance/edit/{id}', 'Service_guidanceController@edit');
Route::post('/guidance/update/{id}', 'Service_guidanceController@update');
Route::delete('/guidance/delete/{id}', 'Service_guidanceController@delete');
// Feeding CRUD
Route::get('/feeding', 'Service_feedingController@index');
Route::post('/feeding/create', 'Service_feedingController@store');
Route::get('/feeding/edit/{id}', 'Service_feedingController@edit');
Route::post('/feeding/update/{id}', 'Service_feedingController@update');
Route::delete('/feeding/delete/{id}', 'Service_feedingController@delete');
// Culture CRUD
Route::get('/culture', 'Service_cultureController@index');
Route::post('/culture/create', 'Service_cultureController@store');
Route::get('/culture/edit/{id}', 'Service_cultureController@edit');
Route::post('/culture/update/{id}', 'Service_cultureController@update');
Route::delete('/culture/delete/{id}', 'Service_cultureController@delete');
// Selects
Route::get('/location_country', 'SelectController@location_country');
Route::get('/location_department', 'SelectController@location_department');
Route::get('/location_township', 'SelectController@location_township');
Route::get('/select_accommodation_for', 'SelectController@select_accommodation_for');
Route::get('/select_in_what_state', 'SelectController@select_in_what_state');
Route::get('/select_how_big_is', 'SelectController@select_how_big_is');
Route::get('/select_type_of_accommodation', 'SelectController@select_type_of_accommodation');
// To Fix
Route::get('/call', 'SelectController@call');
Route::get('/document', 'SelectController@document');