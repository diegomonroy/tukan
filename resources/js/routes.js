// JavaScript Document

/* ************************************************************************************************************************

Tukán

File:			routes.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2019

************************************************************************************************************************ */

import Vue from 'vue';

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

// Pages
import Home from '@/js/pages/Home';
import About_us from '@/js/pages/About_us';
import Calls from '@/js/pages/Calls';
import Documents from '@/js/pages/Documents';
import Contact_us from '@/js/pages/Contact_us';
import Offer_my_services from '@/js/pages/Offer_my_services';
import Growing_up_together from '@/js/pages/Growing_up_together';
import Routes from '@/js/pages/Routes';
import Services from '@/js/pages/Services';
import Item from '@/js/pages/Item';
import Work_with_us from '@/js/pages/Work_with_us';
import Pqr from '@/js/pages/Pqr';
import Terms_and_conditions from '@/js/pages/Terms_and_conditions';
import Sign_in from '@/js/pages/Sign_in';
import Log_in from '@/js/pages/Log_in';
import Profile from '@/js/components/profile_crud/Update';
import RoutesRead from '@/js/components/routes_crud/Read';
import Reports from '@/js/components/report_crud/Read';
import DocumentationUpdate from '@/js/components/documentation_crud/Update';
// Reset Password
import ForgotPassword from '@/js/pages/ForgotPassword';
import ResetPasswordForm from '@/js/pages/ResetPasswordForm';
// Characterization CRUD
import CharacterizationStep1 from '@/js/components/characterization_crud/Step1';
import CharacterizationStep2 from '@/js/components/characterization_crud/Step2';
import CharacterizationRead from '@/js/components/characterization_crud/Read';
import CharacterizationCreate from '@/js/components/characterization_crud/Create';
import CharacterizationUpdate from '@/js/components/characterization_crud/Update';
// Accommodation CRUD
import AccommodationRead from '@/js/components/accommodation_crud/Read';
import AccommodationCreate from '@/js/components/accommodation_crud/Create';
import AccommodationUpdate from '@/js/components/accommodation_crud/Update';
import AccommodationRates from '@/js/components/accommodation_crud/rates/Rates';
// Transport CRUD
import TransportRead from '@/js/components/transport_crud/Read';
import TransportCreate from '@/js/components/transport_crud/Create';
import TransportUpdate from '@/js/components/transport_crud/Update';
// Guidance CRUD
import GuidanceRead from '@/js/components/guidance_crud/Read';
import GuidanceCreate from '@/js/components/guidance_crud/Create';
import GuidanceUpdate from '@/js/components/guidance_crud/Update';
// Feeding CRUD
import FeedingRead from '@/js/components/feeding_crud/Read';
import FeedingCreate from '@/js/components/feeding_crud/Create';
import FeedingUpdate from '@/js/components/feeding_crud/Update';
// Culture CRUD
import CultureRead from '@/js/components/culture_crud/Read';
import CultureCreate from '@/js/components/culture_crud/Create';
import CultureUpdate from '@/js/components/culture_crud/Update';

import Dashboard from './pages/user/Dashboard'
import AdminDashboard from './pages/admin/Dashboard'

const router = new VueRouter({
	history: true,
	mode: 'history',
	routes: [
		{
			path: '/',
			name: 'home',
			component: Home,
			meta: {
				auth: undefined
			}
		},
		{
			path: '/sobre-nosotros',
			name: 'about-us',
			component: About_us,
			meta: {
				auth: undefined
			}
		},
		{
			path: '/convocatorias',
			name: 'calls',
			component: Calls,
			meta: {
				auth: undefined
			}
		},
		{
			path: '/documentos',
			name: 'documents',
			component: Documents,
			meta: {
				auth: undefined
			}
		},
		{
			path: '/contactenos',
			name: 'contact-us',
			component: Contact_us,
			meta: {
				auth: undefined
			}
		},
		{
			path: '/ofrecer-mis-servicios',
			name: 'offer-my-services',
			component: Offer_my_services,
			meta: {
				auth: undefined
			}
		},
		{
			path: '/creciendo-juntos',
			name: 'growing-up-together',
			component: Growing_up_together,
			meta: {
				auth: undefined
			}
		},
		{
			path: '/nuestras-rutas',
			name: 'routes',
			component: Routes,
			meta: {
				auth: undefined
			}
		},
		{
			path: '/nuestros-servicios',
			name: 'services',
			component: Services,
			meta: {
				auth: undefined
			}
		},
		{
			path: '/item',
			name: 'item',
			component: Item,
			meta: {
				auth: undefined
			}
		},
		{
			path: '/trabaja-con-nosotros',
			name: 'work-with-us',
			component: Work_with_us,
			meta: {
				auth: undefined
			}
		},
		{
			path: '/pqr',
			name: 'pqr',
			component: Pqr,
			meta: {
				auth: undefined
			}
		},
		{
			path: '/terminos-y-condiciones',
			name: 'terms-and-conditions',
			component: Terms_and_conditions,
			meta: {
				auth: undefined
			}
		},
		{
			path: '/registrarse',
			name: 'sign-in',
			component: Sign_in,
			meta: {
				auth: false
			}
		},
		{
			path: '/iniciar-sesion',
			name: 'log-in',
			component: Log_in,
			meta: {
				auth: false
			}
		},
		{
			path: '/perfil',
			name: 'profile',
			component: Profile,
			meta: {
				auth: true
			}
		},
		{
			path: '/routes/read',
			name: 'routes-read',
			component: RoutesRead,
			meta: {
				auth: true
			}
		},
		{
			path: '/reportes',
			name: 'reports',
			component: Reports,
			meta: {
				auth: false
			}
		},
		{
			path: '/documentation/update',
			name: 'documentation-update',
			component: DocumentationUpdate,
			meta: {
				auth: false
			}
		},
		// Characterization CRUD
		{
			path: '/characterization/step-1',
			name: 'characterization-step-1',
			component: CharacterizationStep1,
			meta: {
				auth: true
			}
		},
		{
			path: '/characterization/step-2/:id',
			name: 'characterization-step-2',
			component: CharacterizationStep2,
			meta: {
				auth: true
			}
		},
		{
			path: '/characterization/read',
			name: 'characterization-read',
			component: CharacterizationRead,
			meta: {
				auth: false
			}
		},
		{
			path: '/characterization/create',
			name: 'characterization-create',
			component: CharacterizationCreate,
			meta: {
				auth: true
			}
		},
		{
			path: '/characterization/update',
			name: 'characterization-update',
			component: CharacterizationUpdate,
			meta: {
				auth: false
			}
		},
		// Accommodation CRUD
		{
			path: '/accommodation/read',
			name: 'accommodation-read',
			component: AccommodationRead,
			meta: {
				auth: true
			}
		},
		{
			path: '/accommodation/create',
			name: 'accommodation-create',
			component: AccommodationCreate,
			meta: {
				auth: true
			}
		},
		{
			path: '/accommodation/update/:id',
			name: 'accommodation-update',
			component: AccommodationUpdate,
			meta: {
				auth: true
			}
		},
		{
			path: '/accommodation/rates/:id',
			name: 'accommodation-rates',
			component: AccommodationRates,
			meta: {
				auth: true
			}
		},
		// Transport CRUD
		{
			path: '/transport/read',
			name: 'transport-read',
			component: TransportRead,
			meta: {
				auth: true
			}
		},
		{
			path: '/transport/create',
			name: 'transport-create',
			component: TransportCreate,
			meta: {
				auth: true
			}
		},
		{
			path: '/transport/update/:id',
			name: 'transport-update',
			component: TransportUpdate,
			meta: {
				auth: true
			}
		},
		// Guidance CRUD
		{
			path: '/guidance/read',
			name: 'guidance-read',
			component: GuidanceRead,
			meta: {
				auth: true
			}
		},
		{
			path: '/guidance/create',
			name: 'guidance-create',
			component: GuidanceCreate,
			meta: {
				auth: true
			}
		},
		{
			path: '/guidance/update/:id',
			name: 'guidance-update',
			component: GuidanceUpdate,
			meta: {
				auth: true
			}
		},
		// Feeding CRUD
		{
			path: '/feeding/read',
			name: 'feeding-read',
			component: FeedingRead,
			meta: {
				auth: true
			}
		},
		{
			path: '/feeding/create',
			name: 'feeding-create',
			component: FeedingCreate,
			meta: {
				auth: true
			}
		},
		{
			path: '/feeding/update/:id',
			name: 'feeding-update',
			component: FeedingUpdate,
			meta: {
				auth: true
			}
		},
		// Culture CRUD
		{
			path: '/culture/read',
			name: 'culture-read',
			component: CultureRead,
			meta: {
				auth: true
			}
		},
		{
			path: '/culture/create',
			name: 'culture-create',
			component: CultureCreate,
			meta: {
				auth: true
			}
		},
		{
			path: '/culture/update/:id',
			name: 'culture-update',
			component: CultureUpdate,
			meta: {
				auth: true
			}
		},
		// Reset Password
		{
			path: '/reset-password',
			name: 'reset-password',
			component: ForgotPassword,
			meta: {
				auth: false
			}
		},
		{
			path: '/reset-password/:token',
			name: 'reset-password-form',
			component: ResetPasswordForm,
			meta: {
				auth: false
			}
		},
// USER ROUTES
{
path: '/dashboard',
name: 'dashboard',
component: Dashboard,
meta: {
auth: true
}
},
// ADMIN ROUTES
{
path: '/admin',
name: 'admin.dashboard',
component: AdminDashboard,
meta: {
auth: {roles: 2, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
}
}
]
});

export default router;