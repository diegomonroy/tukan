// JavaScript Document

/* ************************************************************************************************************************

Tukán

File:			app.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2019

************************************************************************************************************************ */

/* Foundation */

require('./foundation');

/* Vue.js */

import 'es6-promise/auto';
import axios from 'axios';
import Vue from 'vue';
import VueAuth from '@websanova/vue-auth';
import VueAxios from 'vue-axios';
import VueRouter from 'vue-router';

// Auth
import auth from '@/js/auth.js';

// Routes
import Routes from '@/js/routes.js';

// Component
import App from '@/js/layouts/App';

window.Vue = Vue;

Vue.router = Routes;
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
//axios.defaults.baseURL = `${process.env.MIX_APP_URL}/api`;
axios.defaults.baseURL = `/api`;
Vue.use(VueAuth, auth);

const app = new Vue({
	el: '#app',
	router: Routes,
	render: h => h(App),
});

export default app;