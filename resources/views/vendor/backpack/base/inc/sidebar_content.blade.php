<!-- Begin Dashboard -->
	<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard" aria-hidden="true"></i> <span>{{ trans('general.dashboard') }}</span></a></li>
<!-- End Dashboard -->
<!-- Begin Tukán -->
	<li class="treeview">
		<a href="#"><i class="fa fa-circle" aria-hidden="true"></i> <span>{{ trans('general.tukan') }}</span> <i class="fa fa-angle-left pull-right" aria-hidden="true"></i></a>
		<ul class="treeview-menu">
			@can('Ver Caracterizaciones')
			<li><a href="{{ backpack_url('characterization') }}"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> <span>{{ trans('general.characterizations') }}</span></a></li>
			@endcan
			@can('Ver Alojamientos')
			<li><a href="{{ backpack_url('service_accommodation') }}"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> <span>{{ trans('general.service_accommodations') }}</span></a></li>
			@endcan
			@can('Ver Transportes')
			<li><a href="{{ backpack_url('service_transport') }}"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> <span>{{ trans('general.service_transports') }}</span></a></li>
			@endcan
			@can('Ver Guianzas')
			<li><a href="{{ backpack_url('service_guidance') }}"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> <span>{{ trans('general.service_guidances') }}</span></a></li>
			@endcan
			@can('Ver Alimentaciones')
			<li><a href="{{ backpack_url('service_feeding') }}"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> <span>{{ trans('general.service_feedings') }}</span></a></li>
			@endcan
			@can('Ver Culturas')
			<li><a href="{{ backpack_url('service_culture') }}"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> <span>{{ trans('general.service_cultures') }}</span></a></li>
			@endcan
			@can('Ver Convocatorias')
			<li><a href="{{ backpack_url('call') }}"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> <span>{{ trans('general.calls') }}</span></a></li>
			@endcan
			@can('Ver Documentos')
			<li><a href="{{ backpack_url('document') }}"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> <span>{{ trans('general.documents') }}</span></a></li>
			@endcan
		</ul>
	</li>
<!-- End Tukán -->
<!-- Begin Dropdowns -->
	@can('Ver Desplegables')
	<li class="treeview">
		<a href="#"><i class="fa fa-caret-square-o-down" aria-hidden="true"></i> <span>{{ trans('general.dropdowns') }}</span> <i class="fa fa-angle-left pull-right" aria-hidden="true"></i></a>
		<ul class="treeview-menu">
			<li><a href="{{ backpack_url('location_country') }}"><i class="fa fa-sort-desc" aria-hidden="true"></i> <span>{{ trans('general.location_countries') }}</span></a></li>
			<li><a href="{{ backpack_url('location_department') }}"><i class="fa fa-sort-desc" aria-hidden="true"></i> <span>{{ trans('general.location_departments') }}</span></a></li>
			<li><a href="{{ backpack_url('location_township') }}"><i class="fa fa-sort-desc" aria-hidden="true"></i> <span>{{ trans('general.location_townships') }}</span></a></li>
			<li><a href="{{ backpack_url('location_village') }}"><i class="fa fa-sort-desc" aria-hidden="true"></i> <span>{{ trans('general.location_villages') }}</span></a></li>
			<li><a href="{{ backpack_url('select_in_what_state') }}"><i class="fa fa-sort-desc" aria-hidden="true"></i> <span>{{ trans('general.select_in_what_states') }}</span></a></li>
			<li><a href="{{ backpack_url('select_how_big_is') }}"><i class="fa fa-sort-desc" aria-hidden="true"></i> <span>{{ trans('general.select_how_big_iss') }}</span></a></li>
			<li><a href="{{ backpack_url('select_type_of_tourism') }}"><i class="fa fa-sort-desc" aria-hidden="true"></i> <span>{{ trans('general.select_type_of_tourisms') }}</span></a></li>
			<li><a href="{{ backpack_url('select_ethnic_approach') }}"><i class="fa fa-sort-desc" aria-hidden="true"></i> <span>{{ trans('general.select_ethnic_approaches') }}</span></a></li>
			<li><a href="{{ backpack_url('select_age_approach') }}"><i class="fa fa-sort-desc" aria-hidden="true"></i> <span>{{ trans('general.select_age_approaches') }}</span></a></li>
			<li><a href="{{ backpack_url('select_zone') }}"><i class="fa fa-sort-desc" aria-hidden="true"></i> <span>{{ trans('general.select_zones') }}</span></a></li>
			<li><a href="{{ backpack_url('select_payment_method') }}"><i class="fa fa-sort-desc" aria-hidden="true"></i> <span>{{ trans('general.select_payment_methods') }}</span></a></li>
			<li><a href="{{ backpack_url('select_type_of_accommodation') }}"><i class="fa fa-sort-desc" aria-hidden="true"></i> <span>{{ trans('general.select_type_of_accommodations') }}</span></a></li>
			<li><a href="{{ backpack_url('select_accommodation_for') }}"><i class="fa fa-sort-desc" aria-hidden="true"></i> <span>{{ trans('general.select_accommodation_fors') }}</span></a></li>
			<li><a href="{{ backpack_url('select_type_of_transport') }}"><i class="fa fa-sort-desc" aria-hidden="true"></i> <span>{{ trans('general.select_type_of_transports') }}</span></a></li>
			<li><a href="{{ backpack_url('select_relevant_documentation') }}"><i class="fa fa-sort-desc" aria-hidden="true"></i> <span>{{ trans('general.select_relevant_documentations') }}</span></a></li>
			<li><a href="{{ backpack_url('select_accident_insurance') }}"><i class="fa fa-sort-desc" aria-hidden="true"></i> <span>{{ trans('general.select_accident_insurances') }}</span></a></li>
			<li><a href="{{ backpack_url('select_type_of_feeding') }}"><i class="fa fa-sort-desc" aria-hidden="true"></i> <span>{{ trans('general.select_type_of_feedings') }}</span></a></li>
			<li><a href="{{ backpack_url('select_feeding_preferential') }}"><i class="fa fa-sort-desc" aria-hidden="true"></i> <span>{{ trans('general.select_feeding_preferentials') }}</span></a></li>
		</ul>
	</li>
	@endcan
<!-- End Dropdowns -->
<!-- Begin CMS -->
	@can('Ver CMS')
	<li class="treeview">
		<a href="#"><i class="fa fa-chrome" aria-hidden="true"></i> <span>{{ trans('general.cms') }}</span> <i class="fa fa-angle-left pull-right" aria-hidden="true"></i></a>
		<ul class="treeview-menu">
			<li><a href="{{ backpack_url('page') }}"><i class="fa fa-file-o" aria-hidden="true"></i> <span>{{ trans('general.pages') }}</span></a></li>
			<li><a href="{{ backpack_url('menu-item') }}"><i class="fa fa-list" aria-hidden="true"></i> <span>{{ trans('general.menus') }}</span></a></li>
		</ul>
	</li>
	@endcan
<!-- End CMS -->
<!-- Begin News -->
	@can('Ver Noticias')
	<li class="treeview">
		<a href="#"><i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>{{ trans('general.news') }}</span> <i class="fa fa-angle-left pull-right" aria-hidden="true"></i></a>
		<ul class="treeview-menu">
			<li><a href="{{ backpack_url('article') }}"><i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>{{ trans('general.articles') }}</span></a></li>
			<li><a href="{{ backpack_url('category') }}"><i class="fa fa-list" aria-hidden="true"></i> <span>{{ trans('general.categories') }}</span></a></li>
			<li><a href="{{ backpack_url('tag') }}"><i class="fa fa-tag" aria-hidden="true"></i> <span>{{ trans('general.tags') }}</span></a></li>
		</ul>
	</li>
	@endcan
<!-- End News -->
<!-- Begin Files -->
	@can('Ver Archivos')
	<li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o" aria-hidden="true"></i> <span>{{ trans('general.files') }}</span></a></li>
	@endcan
<!-- End Files -->
<!-- Begin Translations -->
	@can('Ver Traducciones')
	<li class="treeview">
		<a href="#"><i class="fa fa-globe" aria-hidden="true"></i> <span>{{ trans('general.translations') }}</span> <i class="fa fa-angle-left pull-right" aria-hidden="true"></i></a>
		<ul class="treeview-menu">
			<li><a href="{{ backpack_url('language') }}"><i class="fa fa-flag-checkered" aria-hidden="true"></i> <span>{{ trans('general.languages') }}</span></a></li>
			<li><a href="{{ backpack_url('language/texts') }}"><i class="fa fa-language" aria-hidden="true"></i> <span>{{ trans('general.language_files') }}</span></a></li>
		</ul>
	</li>
	@endcan
<!-- End Translations -->
<!-- Begin Users -->
	@can('Ver Usuarios')
	<li class="treeview">
		<a href="#"><i class="fa fa-group" aria-hidden="true"></i> <span>{{ trans('general.users') }}</span> <i class="fa fa-angle-left pull-right" aria-hidden="true"></i></a>
		<ul class="treeview-menu">
			<li><a href="{{ backpack_url('user') }}"><i class="fa fa-user" aria-hidden="true"></i> <span>{{ trans('general.users') }}</span></a></li>
			<li><a href="{{ backpack_url('role') }}"><i class="fa fa-group" aria-hidden="true"></i> <span>{{ trans('general.roles') }}</span></a></li>
			<li><a href="{{ backpack_url('permission') }}"><i class="fa fa-key" aria-hidden="true"></i> <span>{{ trans('general.permissions') }}</span></a></li>
		</ul>
	</li>
	@endcan
<!-- End Users -->
<!-- Begin Configuration -->
	@can('Ver Configuración')
	<li class="treeview">
		<a href="#"><i class="fa fa-cogs" aria-hidden="true"></i> <span>{{ trans('general.configuration') }}</span> <i class="fa fa-angle-left pull-right" aria-hidden="true"></i></a>
		<ul class="treeview-menu">
			<li><a href="{{ backpack_url('setting') }}"><i class="fa fa-cog" aria-hidden="true"></i> <span>{{ trans('general.settings') }}</span></a></li>
			<li><a href="{{ backpack_url('backup') }}"><i class="fa fa-hdd-o" aria-hidden="true"></i> <span>{{ trans('general.backups') }}</span></a></li>
			<li><a href="{{ backpack_url('log') }}"><i class="fa fa-terminal" aria-hidden="true"></i> <span>{{ trans('general.logs') }}</span></a></li>
		</ul>
	</li>
	@endcan
<!-- End Configuration -->