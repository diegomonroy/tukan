<!--

/* ************************************************************************************************************************

Tukán

File:			index.html
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2019

************************************************************************************************************************ */

-->
<!DOCTYPE html>
<html class="no-js" lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>@yield('title', 'Tukán')</title>
		<link rel="shortcut icon" href="/build/favicon.ico">
		<link rel="stylesheet" href="{{ mix('css/app.css') }}">
		<link rel="stylesheet" href="/build/bower_components/datatables/media/css/jquery.dataTables.min.css">
		<link rel="stylesheet" href="/build/bower_components/fancybox/dist/jquery.fancybox.min.css">
		<link rel="stylesheet" href="/build/bower_components/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="/build/bower_components/jQuery-MultiSelect/jquery.multiselect.css">
		<link rel="stylesheet" href="/build/bower_components/owl.carousel/dist/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="/build/bower_components/owl.carousel/dist/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="/build/app.css">
	</head>
	<body>
		<!-- Begin App -->
			<div id="app"></div>
		<!-- End App -->
		<!-- Begin Main Scripts -->
			<script src="{{ mix('js/app.js') }}"></script>
			<script src="/build/bower_components/what-input/dist/what-input.min.js"></script>
			<script src="/build/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
			<script src="/build/bower_components/fancybox/dist/jquery.fancybox.min.js"></script>
			<script src="/build/bower_components/jQuery-MultiSelect/jquery.multiselect.js"></script>
			<script src="/build/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
			<script src="/build/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>
			<script src="/build/app.js"></script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD14p2oG6FlvlEHCZN0l9dwM7Pg6myrbxE&signed_in=true&callback=initMap"></script>
		<!-- End Main Scripts -->
	</body>
</html>