// JavaScript Document

/* ************************************************************************************************************************

Tukán

File:			app.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2019

************************************************************************************************************************ */

/* $ */

$(document).ready(function () {
	/* Owl Carousel 2 */
	$( '.owl-carousel' ).owlCarousel({
		margin: 10,
		loop: true,
		nav: true,
		autoplay: true,
		responsiveClass: true,
		responsive: {
			0: {
				items: 1,
				nav: true
			},
			600: {
				items: 3,
				nav: true
			},
			1000: {
				items: 5,
				nav: true
			}
		}
	});

howMany = 9;
listButton = $('button.list-view');
gridButton = $('button.grid-view');
wrapper = $('div.wrapper');

div = '<div class="item"><a href="javascript:void(0);"><img src="http://development-02-diegomonroy.codeanyapp.com/tukan-test/build/block_2_1.svg" /></a><div class="details"><h2>Titulo de la Ruta</h2><p>Descripción corta del servicio.</p><ul><li>Item 1</li><li>Item 2</li><li>Item 3</li><li>Item 4</li><li>Item 5</li>								</ul><p class="text-center"><a href="item" class="button">Leer más</a></p></div></div>';

// Set up divs
for (i = 0; i < howMany; i++) { 
	$('.wrapper').append( div );  
}

listButton.on('click',function(){
	
  gridButton.removeClass('on');
  listButton.addClass('on');
  wrapper.removeClass('grid').addClass('list');
  
});

gridButton.on('click',function(){
	
  listButton.removeClass('on');
  gridButton.addClass('on');
  wrapper.removeClass('list').addClass('grid');
  
});

});

function initMap() {
var myLatLng = {lat: 4.7552385, lng: -74.0182495};

var map = new google.maps.Map(document.getElementById('map'), {
zoom: 12,
center: myLatLng
});

var marker = new google.maps.Marker({
position: myLatLng,
map: map,
title: 'Hello World!'
});
}