<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;
use Venturecraft\Revisionable\RevisionableTrait;

class Document extends Model
{
    use CrudTrait;
	use HasTranslations;
	use RevisionableTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'documents';
	protected $primaryKey = 'id';
	public $timestamps = true;
    // protected $guarded = ['id'];
	protected $fillable = [
		'name',
		'author',
		'year',
		'file',
'location_country_1_id',
'location_department_1_id',
'location_township_1_id',
'location_country_2_id',
'location_department_2_id',
'location_township_2_id',
'location_country_3_id',
'location_department_3_id',
'location_township_3_id',
'location_country_4_id',
'location_department_4_id',
'location_township_4_id',
'location_country_5_id',
'location_department_5_id',
'location_township_5_id',
'tourism_tags',
'topic_tags',
'source',
		'created_by',
		'updated_by'
	];
	protected $translatable = [
		'name'
	];
	protected $hidden = [
		'created_by',
		'updated_by'
	];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

	public static function boot() {
		parent::boot();
	}

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

	/* Country */
	public function country() {
		return $this->belongsTo('App\Models\Location_country', 'location_country_id');
	}

	/* Department */
	public function department() {
		return $this->belongsTo('App\Models\Location_department', 'location_department_id');
	}

	/* Township */
	public function township() {
		return $this->belongsTo('App\Models\Location_township', 'location_township_id');
	}

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
