<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;
use Venturecraft\Revisionable\RevisionableTrait;

class Service_guidance extends Model
{
    use CrudTrait;
	use HasTranslations;
	use RevisionableTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'service_guidances';
	protected $primaryKey = 'id';
	public $timestamps = true;
    // protected $guarded = ['id'];
	protected $fillable = [
		'characterization_id',
		'name',
		'type_of_guidance',
		'description',
		'specifications',
		'minimum_of_travelers',
		'maximum_of_travelers',
		'share_some_photos',
		'share_some_videos',
		'location',
		'teaching_material',
		'teaching_material_file',
		'accreditation',
		'professional_card',
		'created_by',
		'updated_by'
	];
	protected $translatable = [
		'name',
		'type_of_guidance',
		'description',
		'specifications'
	];
	protected $hidden = [
		'created_by',
		'updated_by'
	];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

	public static function boot() {
		parent::boot();
	}

	/* Characterization */
	public function characterization() {
		return $this->belongsTo('App\Models\Characterization', 'characterization_id');
	}

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
