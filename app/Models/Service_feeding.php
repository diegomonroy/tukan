<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;
use Venturecraft\Revisionable\RevisionableTrait;

class Service_feeding extends Model
{
    use CrudTrait;
	use HasTranslations;
	use RevisionableTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'service_feedings';
	protected $primaryKey = 'id';
	public $timestamps = true;
    // protected $guarded = ['id'];
    protected $fillable = [
		'characterization_id',
		'name',
		'type_of_feeding_id',
		'description',
		'specifications',
		'feeding_preferential_id',
		'share_some_photos',
		'share_some_videos',
		'location',
		'food_handling',
		'drinking_water',
		'electric_power',
		'created_by',
		'updated_by'
	];
	protected $translatable = [
		'name',
		'description',
		'specifications'
	];
	protected $hidden = [
		'created_by',
		'updated_by'
	];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

	public static function boot() {
		parent::boot();
	}

	/* Characterization */
	public function characterization() {
		return $this->belongsTo('App\Models\Characterization', 'characterization_id');
	}

	/* Type of Feeding */
	public function type_of_feeding() {
		return $this->belongsTo('App\Models\Select_type_of_feeding', 'type_of_feeding_id');
	}

	/* Feeding Preferential */
	public function feeding_preferential() {
		return $this->belongsTo('App\Models\Select_feeding_preferential', 'feeding_preferential_id');
	}

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
