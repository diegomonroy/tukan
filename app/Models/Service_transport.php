<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;
use Venturecraft\Revisionable\RevisionableTrait;

class Service_transport extends Model
{
    use CrudTrait;
	use HasTranslations;
	use RevisionableTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'service_transports';
	protected $primaryKey = 'id';
	public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
		'characterization_id',
		'name',
		'type_of_transport_id',
		'description',
		'specifications',
		'minimum_of_travelers',
		'maximum_of_travelers',
		'share_some_photos',
		'share_some_videos',
		'location',
		'security_devices',
		'relevant_documentation_id',
		'accident_insurance_id',
		'created_by',
		'updated_by'
	];
	protected $translatable = [
		'name',
		'description',
		'specifications'
	];
	protected $hidden = [
		'created_by',
		'updated_by'
	];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

	public static function boot() {
		parent::boot();
	}

	/* Characterization */
	public function characterization() {
		return $this->belongsTo('App\Models\Characterization', 'characterization_id');
	}

	/* Type of Transport */
	public function type_of_transport() {
		return $this->belongsTo('App\Models\Select_type_of_transport', 'type_of_transport_id');
	}

	/* Relevant Documentation */
	public function relevant_documentation() {
		return $this->belongsTo('App\Models\Select_relevant_documentation', 'relevant_documentation_id');
	}

	/* Accident Insurance */
	public function accident_insurance() {
		return $this->belongsTo('App\Models\Select_accident_insurance', 'accident_insurance_id');
	}

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
