<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;
use Venturecraft\Revisionable\RevisionableTrait;

class Service_accommodation extends Model
{
    use CrudTrait;
	use HasTranslations;
	use RevisionableTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'service_accommodations';
	protected $primaryKey = 'id';
	public $timestamps = true;
    // protected $guarded = ['id'];
    protected $fillable = [
		'characterization_id',
		'name',
		'type_of_accommodation_id',
		'description',
		'minimum_of_travelers',
		'maximum_of_travelers',
		'share_some_photos',
		'share_some_videos',
		'location',
		'air_conditioner',
		'health_services',
		'drinking_water',
		'electric_power',
		'telephony',
		'internet',
		'accommodation_for_id',
		'people_in_total',
		'simple_accommodation',
		'double_accommodation',
		'shared_accommodation',
		'other_types_of_accommodation',
		'created_by',
		'updated_by'
	];
	protected $translatable = [
		'name',
		'description'
	];
	protected $hidden = [
		'created_by',
		'updated_by'
	];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

	public static function boot() {
		parent::boot();
	}

	/* Characterization */
	public function characterization() {
		return $this->belongsTo('App\Models\Characterization', 'characterization_id');
	}

	/* Type of Accommodation */
	public function type_of_accommodation() {
		return $this->belongsTo('App\Models\Select_type_of_accommodation', 'type_of_accommodation_id');
	}

	/* Accommodation For */
	public function accommodation_for() {
		return $this->belongsTo('App\Models\Select_accommodation_for', 'accommodation_for_id');
	}

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
