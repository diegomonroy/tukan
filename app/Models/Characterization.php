<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;
use Venturecraft\Revisionable\RevisionableTrait;

class Characterization extends Model
{
    use CrudTrait;
	use HasTranslations;
	use RevisionableTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'characterizations';
	protected $primaryKey = 'id';
	public $timestamps = true;
    // protected $guarded = ['id'];
	protected $fillable = [
		'email',
		'first_name',
		'last_name',
		'phone',
		'organization_name',
		'organization_description',
		'logo',
		'share_some_photos',
		'in_what_state_id',
		'since_when',
		'how_big_is_id',
		'location_country_id',
		'location_department_id',
		'location_township_id',
		'organization_address',
		'website',
		'facebook',
		'instagram',
		'twitter',
		'youtube',
		'other_social_networks',
		'type_of_tourism_id',
		'ethnic_approach_id',
		'age_approach_id',
		'reduced_mobility',
		'zone_id',
		'volunteer',
		'created_by',
		'updated_by'
	];
	protected $translatable = [
		'organization_description'
	];
	protected $hidden = [
		'created_by',
		'updated_by'
	];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

	public static function boot() {
		parent::boot();
	}

	/* In What State */
	public function in_what_state() {
		return $this->belongsTo('App\Models\Select_in_what_state', 'in_what_state_id');
	}

	/* How Big Is */
	public function how_big_is() {
		return $this->belongsTo('App\Models\Select_how_big_is', 'how_big_is_id');
	}

	/* Country */
	public function country() {
		return $this->belongsTo('App\Models\Location_country', 'location_country_id');
	}

	/* Department */
	public function department() {
		return $this->belongsTo('App\Models\Location_department', 'location_department_id');
	}

	/* Township */
	public function township() {
		return $this->belongsTo('App\Models\Location_township', 'location_township_id');
	}

	/* Type of Tourism */
	public function type_of_tourism() {
		return $this->belongsTo('App\Models\Select_type_of_tourism', 'type_of_tourism_id');
	}

	/* Ethnic Approach */
	public function ethnic_approach() {
		return $this->belongsTo('App\Models\Select_ethnic_approach', 'ethnic_approach_id');
	}

	/* Age Approach */
	public function age_approach() {
		return $this->belongsTo('App\Models\Select_age_approach', 'age_approach_id');
	}

	/* Zone */
	public function zone() {
		return $this->belongsTo('App\Models\Select_zone', 'zone_id');
	}

	/* Payment Method */
	public function payment_method() {
		return $this->belongsTo('App\Models\Select_payment_method', 'payment_method_id');
	}

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
