<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;
use Venturecraft\Revisionable\RevisionableTrait;

class Call extends Model
{
    use CrudTrait;
	use HasTranslations;
	use RevisionableTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'calls';
	protected $primaryKey = 'id';
	public $timestamps = true;
    // protected $guarded = ['id'];
	protected $fillable = [
		'name',
		'type',
		'objective',
		'features',
		'what_do_you_need',
		'deadline',
		'link',
		'created_by',
		'updated_by'
	];
	protected $translatable = [
		'name',
		'objective',
		'features',
		'what_do_you_need'
	];
    protected $hidden = [
		'created_by',
		'updated_by'
	];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

	public static function boot() {
		parent::boot();
	}

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
