<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Service_transportCollection;
use App\Models\Service_transport;

class Service_transportController extends Controller
{

	/*
	 * Read
	 */
	public function index() {
		return new Service_transportCollection( Service_transport::all() );
	}

	/*
	 * Create
	 */
	public function store( Request $request ) {
		$item = new Service_transport([
			'characterization_id' => 1,
			'name' => $request->get('name'),
			'type_of_transport_id' => $request->get('type_of_transport_id'),
			'description' => $request->get('description'),
			'specifications' => $request->get('specifications'),
			'minimum_of_travelers' => $request->get('minimum_of_travelers'),
			'maximum_of_travelers' => $request->get('maximum_of_travelers'),
			'share_some_photos' => $request->get('share_some_photos'),
			'share_some_videos' => $request->get('share_some_videos'),
			'location' => $request->get('location'),
			'security_devices' => $request->get('security_devices'),
			'relevant_documentation_id' => $request->get('relevant_documentation_id'),
			'accident_insurance_id' => $request->get('accident_insurance_id'),
		]);
		$item->save();
		return response()->json('Successfully Added');
	}

	/*
	 * Edit
	 */
	public function edit( $id ) {
		$item = Service_transport::find( $id );
		return response()->json($item);
	}

	/*
	 * Update
	 */
	public function update( $id, Request $request ) {
		$item = Service_transport::find( $id );
		$item->update($request->all());
		return response()->json('Successfully Updated');
	}

	/*
	 * Delete
	 */
	public function delete( $id ) {
		$item = Service_transport::find( $id );
		$item->delete();
		return response()->json('Successfully Deleted');
	}

}
