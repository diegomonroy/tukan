<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Service_guidanceCollection;
use App\Models\Service_guidance;

class Service_guidanceController extends Controller
{

	/*
	 * Read
	 */
	public function index() {
		return new Service_guidanceCollection( Service_guidance::all() );
	}

	/*
	 * Create
	 */
	public function store( Request $request ) {
		$item = new Service_guidance([
			'characterization_id' => 1,
			'name' => $request->get('name'),
			'type_of_guidance' => $request->get('type_of_guidance'),
			'description' => $request->get('description'),
			'specifications' => $request->get('specifications'),
			'minimum_of_travelers' => $request->get('minimum_of_travelers'),
			'maximum_of_travelers' => $request->get('maximum_of_travelers'),
			'share_some_photos' => $request->get('share_some_photos'),
			'share_some_videos' => $request->get('share_some_videos'),
			'location' => $request->get('location'),
			'teaching_material' => $request->get('teaching_material'),
			'teaching_material_file' => $request->get('teaching_material_file'),
			'accreditation' => $request->get('accreditation'),
			'professional_card' => $request->get('professional_card'),
		]);
		$item->save();
		return response()->json('Successfully Added');
	}

	/*
	 * Edit
	 */
	public function edit( $id ) {
		$item = Service_guidance::find( $id );
		return response()->json($item);
	}

	/*
	 * Update
	 */
	public function update( $id, Request $request ) {
		$item = Service_guidance::find( $id );
		$item->update($request->all());
		return response()->json('Successfully Updated');
	}

	/*
	 * Delete
	 */
	public function delete( $id ) {
		$item = Service_guidance::find( $id );
		$item->delete();
		return response()->json('Successfully Deleted');
	}

}
