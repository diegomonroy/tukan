<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Location_country;
use App\Models\Location_department;
use App\Models\Location_township;
use App\Models\Location_village;
use Illuminate\Http\Request;

class LocationController extends Controller
{

	/*
	 * Department
	 */
	public function department( Request $request ) {
		$search_term = $request->input('q');
		$form = collect( $request->input('form') )->pluck('value', 'name');
		$options = Location_department::query();
		if ( ! $form['location_country_id'] ) {
			return [];
		}
		if ( $form['location_country_id'] ) {
			$options = $options->where('country_id', $form['location_country_id']);
		}
		if ( $search_term ) {
			$results = $options->where('name', 'LIKE', '%' . $search_term . '%')->orderBy('name')->paginate(10);
		} else {
			$results = $options->orderBy('name')->paginate(10);
		}
		return $options->orderBy('name')->paginate(10);
	}

	/*
	 * Show Department by id
	 */
	public function department_show( $id ) {
		return Location_department::find( $id );
	}

	/*
	 * Township
	 */
	public function township( Request $request ) {
		$search_term = $request->input('q');
		$form = collect( $request->input('form') )->pluck('value', 'name');
		$options = Location_township::query();
		if ( ! $form['location_department_id'] ) {
			return [];
		}
		if ( $form['location_department_id'] ) {
			$options = $options->where('department_id', $form['location_department_id']);
		}
		if ( $search_term ) {
			$results = $options->where('name', 'LIKE', '%' . $search_term . '%')->orderBy('name')->paginate(10);
		} else {
			$results = $options->orderBy('name')->paginate(10);
		}
		return $options->orderBy('name')->paginate(10);
	}

	/*
	 * Show Township by id
	 */
	public function township_show( $id ) {
		return Location_township::find( $id );
	}

	/*
	 * Village
	 */
	public function village( Request $request ) {
		$search_term = $request->input('q');
		$form = collect( $request->input('form') )->pluck('value', 'name');
		$options = Location_village::query();
		if ( ! $form['location_township_id'] ) {
			return [];
		}
		if ( $form['location_township_id'] ) {
			$options = $options->where('township_id', $form['location_township_id']);
		}
		if ( $search_term ) {
			$results = $options->where('name', 'LIKE', '%' . $search_term . '%')->orderBy('name')->paginate(10);
		} else {
			$results = $options->orderBy('name')->paginate(10);
		}
		return $options->orderBy('name')->paginate(10);
	}

	/*
	 * Show Village by id
	 */
	public function village_show( $id ) {
		return Location_village::find( $id );
	}

}
