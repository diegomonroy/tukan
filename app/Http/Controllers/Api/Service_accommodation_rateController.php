<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Service_accommodation_rate;

class Service_accommodation_rateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return Service_accommodation_rate::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$item = new Service_accommodation_rate();
		$item->service_accommodation_id = 1;
		$item->price = $request->price;
		$item->price_other = $request->price_other;
		$item->since_when = $request->since_when;
		$item->even_when = $request->even_when;
		$item->save();
		return $item;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$item = Service_accommodation_rate::find( $id );
		$item->service_accommodation_id = 1;
		$item->price = $request->price;
		$item->price_other = $request->price_other;
		$item->since_when = $request->since_when;
		$item->even_when = $request->even_when;
		$item->save();
		return $item;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$item = Service_accommodation_rate::find( $id );
		$item->delete();
    }
}
