<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\SelectCollection;
use App\Models\Location_country;
use App\Models\Location_department;
use App\Models\Location_township;
use App\Models\Select_accommodation_for;
use App\Models\Select_in_what_state;
use App\Models\Select_how_big_is;
use App\Models\Select_type_of_accommodation;
// To Fix
use App\Models\Call;
use App\Models\Document;

class SelectController extends Controller
{

	/*
	 * Location_country
	 */
	public function location_country() {
		return new SelectCollection( Location_country::all() );
	}

	/*
	 * Location_department
	 */
	public function location_department() {
		return new SelectCollection( Location_department::all() );
	}

	/*
	 * Location_township
	 */
	public function location_township() {
		return new SelectCollection( Location_township::all() );
	}

	/*
	 * Select_accommodation_for
	 */
	public function select_accommodation_for() {
		return new SelectCollection( Select_accommodation_for::all() );
	}

	/*
	 * Select_in_what_state
	 */
	public function select_in_what_state() {
		return new SelectCollection( Select_in_what_state::all() );
	}

	/*
	 * Select_how_big_is
	 */
	public function select_how_big_is() {
		return new SelectCollection( Select_how_big_is::all() );
	}

	/*
	 * Select_type_of_accommodation
	 */
	public function select_type_of_accommodation() {
		return new SelectCollection( Select_type_of_accommodation::all() );
	}

	/*
	 * To Fix
	 */
	public function call() {
		return new SelectCollection( Call::all() );
	}
	public function document() {
		return new SelectCollection( Document::all() );
	}

}
