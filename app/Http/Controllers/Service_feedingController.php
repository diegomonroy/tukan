<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Service_feedingCollection;
use App\Models\Service_feeding;

class Service_feedingController extends Controller
{

	/*
	 * Read
	 */
	public function index() {
		return new Service_feedingCollection( Service_feeding::all() );
	}

	/*
	 * Create
	 */
	public function store( Request $request ) {
		$item = new Service_feeding([
			'characterization_id' => 1,
			'name' => $request->get('name'),
			'type_of_feeding_id' => $request->get('type_of_feeding_id'),
			'description' => $request->get('description'),
			'specifications' => $request->get('specifications'),
			'feeding_preferential_id' => $request->get('feeding_preferential_id'),
			'share_some_photos' => $request->get('share_some_photos'),
			'share_some_videos' => $request->get('share_some_videos'),
			'location' => $request->get('location'),
			'food_handling' => $request->get('food_handling'),
			'drinking_water' => $request->get('drinking_water'),
			'electric_power' => $request->get('electric_power'),
		]);
		$item->save();
		return response()->json('Successfully Added');
	}

	/*
	 * Edit
	 */
	public function edit( $id ) {
		$item = Service_feeding::find( $id );
		return response()->json($item);
	}

	/*
	 * Update
	 */
	public function update( $id, Request $request ) {
		$item = Service_feeding::find( $id );
		$item->update($request->all());
		return response()->json('Successfully Updated');
	}

	/*
	 * Delete
	 */
	public function delete( $id ) {
		$item = Service_feeding::find( $id );
		$item->delete();
		return response()->json('Successfully Deleted');
	}

}
