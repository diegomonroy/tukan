<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Location_countryRequest as StoreRequest;
use App\Http\Requests\Location_countryRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class Location_countryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class Location_countryCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Location_country');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/location_country');
        $this->crud->setEntityNameStrings(trans('general.location_country'), trans('general.location_countries'));

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();

		/*
		|--------------------------------------------------------------------------
		| FUNCTIONS
		|--------------------------------------------------------------------------
		*/

		$user = backpack_user();

		/*
		|--------------------------------------------------------------------------
		| CRUD
		|--------------------------------------------------------------------------
		*/

		/* ID */

		$this->crud->addColumn([
			'name' => 'id',
			'label' => trans('general.id'),
		]);

		/* País */

		$this->crud->addColumn([
			'name' => 'name',
			'label' => trans('general.country'),
			'limit' => 255,
		]);

		$this->crud->addField([
			'name' => 'name',
			'label' => trans('general.country'),
			'type' => 'text',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Código */

		$this->crud->addColumn([
			'name' => 'code',
			'label' => trans('general.code'),
			'limit' => 255,
		]);

		$this->crud->addField([
			'name' => 'code',
			'label' => trans('general.code'),
			'type' => 'text',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Created By */

		$this->crud->addField([
			'name' => 'created_by',
			'type' => 'hidden',
			'value' => backpack_user()->id,
		], 'create');

		/* Updated By */

		$this->crud->addField([
			'name' => 'updated_by',
			'type' => 'hidden',
			'value' => backpack_user()->id,
		], 'update');

		/*
		|--------------------------------------------------------------------------
		| TABLE
		|--------------------------------------------------------------------------
		*/

		// Revisions
		$this->crud->allowAccess('revisions');
		$this->crud->with('revisionHistory');

		// Export Buttons
		$this->crud->enableExportButtons();

        // add asterisk for fields that are required in Location_countryRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
