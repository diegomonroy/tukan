<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CallRequest as StoreRequest;
use App\Http\Requests\CallRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class CallCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CallCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Call');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/call');
        $this->crud->setEntityNameStrings(trans('general.call'), trans('general.calls'));

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();

		/*
		|--------------------------------------------------------------------------
		| FUNCTIONS
		|--------------------------------------------------------------------------
		*/

		$user = backpack_user();

		/*
		|--------------------------------------------------------------------------
		| CRUD
		|--------------------------------------------------------------------------
		*/

		/* ID */

		$this->crud->addColumn([
			'name' => 'id',
			'label' => trans('general.id'),
		]);

		/* Nombre */

		$this->crud->addColumn([
			'name' => 'name',
			'label' => trans('general.name'),
			'limit' => 255,
		]);

		$this->crud->addField([
			'name' => 'name',
			'label' => trans('general.name'),
			'type' => 'text',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Tipo */

		 $this->crud->addField([
			'name' => 'type',
			'label' => trans('general.type'),
			'type' => 'select_from_array',
			'options' => [
				'' => trans('general.select'),
				'internal' => trans('general.internal'),
				'external' => trans('general.external')
			],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Objetivo */

		$this->crud->addField([
			'name' => 'objective',
			'label' => trans('general.objective'),
			'type' => 'text',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Características */

		$this->crud->addField([
			'name' => 'features',
			'label' => trans('general.features'),
			'type' => 'textarea',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* ¿Qué se Necesita? */

		$this->crud->addField([
			'name' => 'what_do_you_need',
			'label' => trans('general.what_do_you_need'),
			'type' => 'textarea',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Fecha Límite */

		$this->crud->addField([
			'name' => 'deadline',
			'label' => trans('general.deadline'),
			'type' => 'date',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Link a Términos de Referencia */

		$this->crud->addField([
			'name' => 'link',
			'label' => trans('general.link_to_terms_of_reference'),
			'type' => 'url',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Created By */

		$this->crud->addField([
			'name' => 'created_by',
			'type' => 'hidden',
			'value' => backpack_user()->id,
		], 'create');

		/* Updated By */

		$this->crud->addField([
			'name' => 'updated_by',
			'type' => 'hidden',
			'value' => backpack_user()->id,
		], 'update');

		/*
		|--------------------------------------------------------------------------
		| TABLE
		|--------------------------------------------------------------------------
		*/

		// Revisions
		$this->crud->allowAccess('revisions');
		$this->crud->with('revisionHistory');

		// Export Buttons
		$this->crud->enableExportButtons();

		/*
		|--------------------------------------------------------------------------
		| PERMISSIONS
		|--------------------------------------------------------------------------
		*/

		// Read
		if ( $user->hasPermissionTo( 'Ver Convocatorias' ) ) {
			$this->crud->allowAccess( 'list' );
		} else {
			$this->crud->denyAccess( 'list' );
		}

		// Create
		if ( $user->hasPermissionTo( 'Crear Convocatorias' ) ) {
			$this->crud->allowAccess( 'create' );
		} else {
			$this->crud->denyAccess( 'create' );
		}

		// Update
		if ( $user->hasPermissionTo( 'Editar Convocatorias' ) ) {
			$this->crud->allowAccess( 'update' );
		} else {
			$this->crud->denyAccess( 'update' );
		}

		// Delete
		if ( $user->hasPermissionTo( 'Borrar Convocatorias' ) ) {
			$this->crud->allowAccess( 'delete' );
		} else {
			$this->crud->denyAccess( 'delete' );
		}

        // add asterisk for fields that are required in CallRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
