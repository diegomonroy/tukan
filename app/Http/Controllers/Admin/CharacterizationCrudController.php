<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CharacterizationRequest as StoreRequest;
use App\Http\Requests\CharacterizationRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class CharacterizationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CharacterizationCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Characterization');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/characterization');
        $this->crud->setEntityNameStrings(trans('general.characterization'), trans('general.characterizations'));

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();

		/*
		|--------------------------------------------------------------------------
		| FUNCTIONS
		|--------------------------------------------------------------------------
		*/

		$user = backpack_user();

		/*
		|--------------------------------------------------------------------------
		| CRUD
		|--------------------------------------------------------------------------
		*/

		/* ID */

		$this->crud->addColumn([
			'name' => 'id',
			'label' => trans('general.id'),
		]);

		/* Email */

		$this->crud->addColumn([
			'name' => 'email',
			'label' => trans('general.email'),
			'limit' => 255,
		]);

		$this->crud->addField([
			'name' => 'email',
			'label' => trans('general.email'),
			'type' => 'email',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Nombre */

		$this->crud->addColumn([
			'name' => 'first_name',
			'label' => trans('general.first_name'),
			'limit' => 255,
		]);

		$this->crud->addField([
			'name' => 'first_name',
			'label' => trans('general.first_name'),
			'type' => 'text',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-6'
			],
		]);

		/* Apellido */

		$this->crud->addColumn([
			'name' => 'last_name',
			'label' => trans('general.last_name'),
			'limit' => 255,
		]);

		$this->crud->addField([
			'name' => 'last_name',
			'label' => trans('general.last_name'),
			'type' => 'text',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-6'
			],
		]);

		/* Teléfono */

		$this->crud->addField([
			'name' => 'phone',
			'label' => trans('general.phone'),
			'type' => 'number',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Nombre de la Organización / Empresa / Asociación / Persona que Presta los Servicios o Productos Turísticos */

		$this->crud->addField([
			'name' => 'organization_name',
			'label' => trans('general.organization_name'),
			'type' => 'textarea',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Descripción de la Organización / Empresa / Asociación / Persona que Presta los Servicios o Productos Turísticos */

		$this->crud->addField([
			'name' => 'organization_description',
			'label' => trans('general.organization_description'),
			'type' => 'wysiwyg',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Logo */

		$this->crud->addField([
			'name' => 'logo',
			'label' => trans('general.logo'),
			'type' => 'browse',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Comparte unas Fotos de tu Organización con los Viajeros */

		$this->crud->addField([
			'name' => 'share_some_photos',
			'label' => trans('general.share_some_photos_of_your_organization_with_travelers'),
			'type' => 'browse_multiple',
			'mime_types' => ['image'],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* En Qué Estado se Encuentra tu Iniciativa */

		$this->crud->addField([
			'label' => trans('general.in_what_state_is_your_initiative'),
			'type' => 'select2',
			'name' => 'in_what_state_id',
			'entity' => 'in_what_state',
			'attribute' => 'name',
			'model' => 'App\Models\Select_in_what_state',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* ¿Desde Cuándo Está Operando? */

		$this->crud->addField([
			'name' => 'since_when',
			'label' => trans('general.since_when_is_it_operating'),
			'type' => 'date',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* ¿Qué Tamaño Tiene la Organización? */

		$this->crud->addField([
			'label' => trans('general.how_big_is_the_organization'),
			'type' => 'select2',
			'name' => 'how_big_is_id',
			'entity' => 'how_big_is',
			'attribute' => 'name',
			'model' => 'App\Models\Select_how_big_is',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* País */

		$this->crud->addField([
			'label' => trans('general.country'),
			'type' => 'select2',
			'name' => 'location_country_id',
			'entity' => 'country',
			'attribute' => 'name',
			'model' => 'App\Models\Location_country',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Departamento */

		$this->crud->addField([
			'label' => trans('general.department'),
			'type' => 'select2_from_ajax',
			'name' => 'location_department_id',
			'entity' => 'department',
			'attribute' => 'name',
			'data_source' => url('api/department'),
			'placeholder' => trans('general.select'),
			'minimum_input_length' => 0,
			'dependencies' => ['location_country_id'],
			'method' => 'POST',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Municipio */

		$this->crud->addField([
			'label' => trans('general.township'),
			'type' => 'select2_from_ajax',
			'name' => 'location_township_id',
			'entity' => 'township',
			'attribute' => 'name',
			'data_source' => url('api/township'),
			'placeholder' => trans('general.select'),
			'minimum_input_length' => 0,
			'dependencies' => ['location_department_id'],
			'method' => 'POST',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Dirección del Domicilio de la Organización */

		$this->crud->addField([
			'name' => 'organization_address',
			'label' => trans('general.organization_address'),
			'type' => 'text',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Sitio Web */

		$this->crud->addField([
			'name' => 'website',
			'label' => trans('general.website'),
			'type' => 'url',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Facebook */

		$this->crud->addField([
			'name' => 'facebook',
			'label' => trans('general.facebook'),
			'type' => 'url',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Instagram */

		$this->crud->addField([
			'name' => 'instagram',
			'label' => trans('general.instagram'),
			'type' => 'url',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Twitter */

		$this->crud->addField([
			'name' => 'twitter',
			'label' => trans('general.twitter'),
			'type' => 'url',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* YouTube */

		$this->crud->addField([
			'name' => 'youtube',
			'label' => trans('general.youtube'),
			'type' => 'url',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Otras Redes Sociales */

		$this->crud->addField([
			'name' => 'other_social_networks',
			'label' => trans('general.other_social_networks'),
			'type' => 'url',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* ¿Qué Tipo de Turismo Ofreces? */

		$this->crud->addField([
			'label' => trans('general.what_kind_of_tourism_do_you_offer'),
			'type' => 'select2',
			'name' => 'type_of_tourism_id',
			'entity' => 'type_of_tourism',
			'attribute' => 'name',
			'model' => 'App\Models\Select_type_of_tourism',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* ¿Tus Servicios o Productos Turísticos son de Enfoque Étnico? */

		$this->crud->addField([
			'label' => trans('general.are_your_services_or_tourism_products_ethnic'),
			'type' => 'select2',
			'name' => 'ethnic_approach_id',
			'entity' => 'ethnic_approach',
			'attribute' => 'name',
			'model' => 'App\Models\Select_ethnic_approach',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* ¿Tus Servicios o Productos Turísticos Tienen Enfoque por Grupos de Edad? */

		$this->crud->addField([
			'label' => trans('general.do_your_tourism_services_or_products_have_an_approach_by_age_groups'),
			'type' => 'select2',
			'name' => 'age_approach_id',
			'entity' => 'age_approach',
			'attribute' => 'name',
			'model' => 'App\Models\Select_age_approach',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* ¿Tus Servicios o Productos Turísticos Tienen Acceso a Personas con Discapacidad o Movilidad Reducida? */

		 $this->crud->addField([
			'name' => 'reduced_mobility',
			'label' => trans('general.do_your_tourism_services_or_products_have_access_to_people_with_disabilities_or_reduced_mobility'),
			'type' => 'select_from_array',
			'options' => [
				'' => trans('general.select'),
				'yes' => trans('general.yes'),
				'no' => trans('general.no')
			],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Tus Servicios o Productos Turísticos se Brindan en */

		$this->crud->addField([
			'label' => trans('general.your_services_or_tourist_products_are_offered_in'),
			'type' => 'select2',
			'name' => 'zone_id',
			'entity' => 'zone',
			'attribute' => 'name',
			'model' => 'App\Models\Select_zone',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Estarías Dispuesto a Recibir Voluntarios en tu Organización */

		 $this->crud->addField([
			'name' => 'volunteer',
			'label' => trans('general.you_would_be_willing_to_receive_volunteers_in_your_organization'),
			'type' => 'select_from_array',
			'options' => [
				'' => trans('general.select'),
				'yes' => trans('general.yes'),
				'no' => trans('general.no')
			],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Documentación */

		$this->crud->addField([
			'name' => 'separator_1',
			'type' => 'custom_html',
			'value' => '<h3>' . trans('general.documentation') . '<h3><hr>',
		]);

		/* ¿Tu Organización tiene Registro Único Tributario (RUT)? */

		 $this->crud->addField([
			'name' => 'rut',
			'label' => trans('general.does_your_organization_have_a_unique_tax_registry_rut'),
			'type' => 'select_from_array',
			'options' => [
				'' => trans('general.select'),
				'yes' => trans('general.yes'),
				'no' => trans('general.no')
			],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Número RUT */

		$this->crud->addField([
			'name' => 'rut_number',
			'label' => trans('general.rut_number'),
			'type' => 'number',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Adjunta PDF de tu RUT */

		$this->crud->addField([
			'name' => 'rut_file',
			'label' => trans('general.rut_file'),
			'type' => 'browse',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Separator */

		$this->crud->addField([
			'name' => 'separator_2',
			'type' => 'custom_html',
			'value' => '<hr>',
		]);

		/* ¿Tu Organización tiene Registro Nacional de Turismo (RNT)? */

		 $this->crud->addField([
			'name' => 'rnt',
			'label' => trans('general.does_your_organization_have_a_national_tourism_registry_rnt'),
			'type' => 'select_from_array',
			'options' => [
				'' => trans('general.select'),
				'yes' => trans('general.yes'),
				'no' => trans('general.no')
			],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Número RNT */

		$this->crud->addField([
			'name' => 'rnt_number',
			'label' => trans('general.rnt_number'),
			'type' => 'number',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Adjunta PDF de tu RNT */

		$this->crud->addField([
			'name' => 'rnt_file',
			'label' => trans('general.rnt_file'),
			'type' => 'browse',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Separator */

		$this->crud->addField([
			'name' => 'separator_3',
			'type' => 'custom_html',
			'value' => '<hr>',
		]);

		/* ¿Por qué Medio Prefieres que te Hagamos los Pagos? */

		$this->crud->addField([
			'label' => trans('general.why_do_you_prefer_that_we_make_the_payments'),
			'type' => 'select2',
			'name' => 'payment_method_id',
			'entity' => 'payment_method',
			'attribute' => 'name',
			'model' => 'App\Models\Select_payment_method',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* A qué Cuenta Quieres que te Consignemos */

		$this->crud->addField([
			'name' => 'bank_number',
			'label' => trans('general.what_account_do_you_want_us_to_give_you'),
			'type' => 'number',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Giros Efecty */

		$this->crud->addField([
			'name' => 'efecty_number',
			'label' => trans('general.efecty_twists'),
			'type' => 'number',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Separator */

		$this->crud->addField([
			'name' => 'separator_4',
			'type' => 'custom_html',
			'value' => '<hr>',
		]);

		/* Persona de Contacto */

		$this->crud->addField([
			'name' => 'contact_name',
			'label' => trans('general.contact_name'),
			'type' => 'text',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Cédula de Ciudadanía */

		$this->crud->addField([
			'name' => 'contact_document',
			'label' => trans('general.contact_document'),
			'type' => 'number',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Teléfono de Contacto */

		$this->crud->addField([
			'name' => 'contact_phone',
			'label' => trans('general.contact_phone'),
			'type' => 'number',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Created By */

		$this->crud->addField([
			'name' => 'created_by',
			'type' => 'hidden',
			'value' => backpack_user()->id,
		], 'create');

		/* Updated By */

		$this->crud->addField([
			'name' => 'updated_by',
			'type' => 'hidden',
			'value' => backpack_user()->id,
		], 'update');

		/*
		|--------------------------------------------------------------------------
		| TABLE
		|--------------------------------------------------------------------------
		*/

		// Revisions
		$this->crud->allowAccess('revisions');
		$this->crud->with('revisionHistory');

		// Export Buttons
		$this->crud->enableExportButtons();

		/*
		|--------------------------------------------------------------------------
		| PERMISSIONS
		|--------------------------------------------------------------------------
		*/

		// Read
		if ( $user->hasPermissionTo( 'Ver Caracterizaciones' ) ) {
			$this->crud->allowAccess( 'list' );
		} else {
			$this->crud->denyAccess( 'list' );
		}

		// Create
		if ( $user->hasPermissionTo( 'Crear Caracterizaciones' ) ) {
			$this->crud->allowAccess( 'create' );
		} else {
			$this->crud->denyAccess( 'create' );
		}

		// Update
		if ( $user->hasPermissionTo( 'Editar Caracterizaciones' ) ) {
			$this->crud->allowAccess( 'update' );
		} else {
			$this->crud->denyAccess( 'update' );
		}

		// Delete
		if ( $user->hasPermissionTo( 'Borrar Caracterizaciones' ) ) {
			$this->crud->allowAccess( 'delete' );
		} else {
			$this->crud->denyAccess( 'delete' );
		}

        // add asterisk for fields that are required in CharacterizationRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
