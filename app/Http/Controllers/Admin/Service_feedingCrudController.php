<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Service_feedingRequest as StoreRequest;
use App\Http\Requests\Service_feedingRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class Service_feedingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class Service_feedingCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Service_feeding');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/service_feeding');
        $this->crud->setEntityNameStrings(trans('general.service_feeding'), trans('general.service_feedings'));

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();

		/*
		|--------------------------------------------------------------------------
		| FUNCTIONS
		|--------------------------------------------------------------------------
		*/

		$user = backpack_user();

		/*
		|--------------------------------------------------------------------------
		| CRUD
		|--------------------------------------------------------------------------
		*/

		/* ID */

		$this->crud->addColumn([
			'name' => 'id',
			'label' => trans('general.id'),
		]);

		/* Caracterización */

		$this->crud->addColumn([
			'label' => trans('general.characterization'),
			'type' => 'select',
			'name' => 'characterization_id',
			'entity' => 'characterization',
			'attribute' => 'organization_name',
			'model' => 'App\Models\Characterization',
		]);

		$this->crud->addField([
			'label' => trans('general.characterization'),
			'type' => 'select2',
			'name' => 'characterization_id',
			'entity' => 'characterization',
			'attribute' => 'organization_name',
			'model' => 'App\Models\Characterization',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Nombre */

		$this->crud->addColumn([
			'name' => 'name',
			'label' => trans('general.name'),
			'limit' => 255,
		]);

		$this->crud->addField([
			'name' => 'name',
			'label' => trans('general.name'),
			'type' => 'text',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* ¿Qué Servicios de Alimentación Ofreces? */

		$this->crud->addColumn([
			'label' => trans('general.what_food_services_do_you_offer'),
			'type' => 'select',
			'name' => 'type_of_feeding_id',
			'entity' => 'type_of_feeding',
			'attribute' => 'name',
			'model' => 'App\Models\Select_type_of_feeding',
		]);

		$this->crud->addField([
			'label' => trans('general.what_food_services_do_you_offer'),
			'type' => 'select2',
			'name' => 'type_of_feeding_id',
			'entity' => 'type_of_feeding',
			'attribute' => 'name',
			'model' => 'App\Models\Select_type_of_feeding',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Descripción */

		$this->crud->addField([
			'name' => 'description',
			'label' => trans('general.description'),
			'type' => 'textarea',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Especificaciones */

		$this->crud->addField([
			'name' => 'specifications',
			'label' => trans('general.specifications'),
			'type' => 'textarea',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Opciones Preferenciales */

		$this->crud->addField([
			'label' => trans('general.preferential_options'),
			'type' => 'select2',
			'name' => 'feeding_preferential_id',
			'entity' => 'feeding_preferential',
			'attribute' => 'name',
			'model' => 'App\Models\Select_feeding_preferential',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Comparte Algunas Fotos */

		$this->crud->addField([
			'name' => 'share_some_photos',
			'label' => trans('general.share_some_photos'),
			'type' => 'browse_multiple',
			'mime_types' => ['image'],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Comparte Algunos Vídeos */

		$this->crud->addField([
			'name' => 'share_some_videos',
			'label' => trans('general.share_some_videos'),
			'type' => 'browse_multiple',
			'mime_types' => ['image'],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Comparte la Ubicación en Google Maps */

		$this->crud->addField([
			'name' => 'location',
			'label' => trans('general.share_the_location_on_google_maps'),
			'type' => 'text',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Cuentas con Certificado de Manipulación de Alimentos */

		 $this->crud->addField([
			'name' => 'food_handling',
			'label' => trans('general.accounts_with_food_handling_certificate'),
			'type' => 'select_from_array',
			'options' => [
				'' => trans('general.select'),
				'yes' => trans('general.yes'),
				'no' => trans('general.no')
			],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Cuentas con Acceso Permanente a Agua Potable */

		 $this->crud->addField([
			'name' => 'drinking_water',
			'label' => trans('general.accounts_with_permanent_access_to_drinking_water'),
			'type' => 'select_from_array',
			'options' => [
				'' => trans('general.select'),
				'yes' => trans('general.yes'),
				'no' => trans('general.no')
			],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Cuentas con Suministro de Energía Eléctrica Permanente */

		 $this->crud->addField([
			'name' => 'electric_power',
			'label' => trans('general.accounts_with_permanent_power_supply'),
			'type' => 'select_from_array',
			'options' => [
				'' => trans('general.select'),
				'yes' => trans('general.yes'),
				'no' => trans('general.no')
			],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Created By */

		$this->crud->addField([
			'name' => 'created_by',
			'type' => 'hidden',
			'value' => backpack_user()->id,
		], 'create');

		/* Updated By */

		$this->crud->addField([
			'name' => 'updated_by',
			'type' => 'hidden',
			'value' => backpack_user()->id,
		], 'update');

		/*
		|--------------------------------------------------------------------------
		| TABLE
		|--------------------------------------------------------------------------
		*/

		// Revisions
		$this->crud->allowAccess('revisions');
		$this->crud->with('revisionHistory');

		// Export Buttons
		$this->crud->enableExportButtons();

		/*
		|--------------------------------------------------------------------------
		| PERMISSIONS
		|--------------------------------------------------------------------------
		*/

		// Read
		if ( $user->hasPermissionTo( 'Ver Alimentaciones' ) ) {
			$this->crud->allowAccess( 'list' );
		} else {
			$this->crud->denyAccess( 'list' );
		}

		// Create
		if ( $user->hasPermissionTo( 'Crear Alimentaciones' ) ) {
			$this->crud->allowAccess( 'create' );
		} else {
			$this->crud->denyAccess( 'create' );
		}

		// Update
		if ( $user->hasPermissionTo( 'Editar Alimentaciones' ) ) {
			$this->crud->allowAccess( 'update' );
		} else {
			$this->crud->denyAccess( 'update' );
		}

		// Delete
		if ( $user->hasPermissionTo( 'Borrar Alimentaciones' ) ) {
			$this->crud->allowAccess( 'delete' );
		} else {
			$this->crud->denyAccess( 'delete' );
		}

        // add asterisk for fields that are required in Service_feedingRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
