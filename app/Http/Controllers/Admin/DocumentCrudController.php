<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\DocumentRequest as StoreRequest;
use App\Http\Requests\DocumentRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class DocumentCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class DocumentCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Document');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/document');
        $this->crud->setEntityNameStrings(trans('general.document'), trans('general.documents'));

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();

		/*
		|--------------------------------------------------------------------------
		| FUNCTIONS
		|--------------------------------------------------------------------------
		*/

		$user = backpack_user();

		/*
		|--------------------------------------------------------------------------
		| CRUD
		|--------------------------------------------------------------------------
		*/

		/* ID */

		$this->crud->addColumn([
			'name' => 'id',
			'label' => trans('general.id'),
		]);

		/* Título */

		$this->crud->addColumn([
			'name' => 'name',
			'label' => trans('general.title'),
			'limit' => 255,
		]);

		$this->crud->addField([
			'name' => 'name',
			'label' => trans('general.title'),
			'type' => 'text',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Autor */

		$this->crud->addField([
			'name' => 'author',
			'label' => trans('general.author'),
			'type' => 'text',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Año */

		$this->crud->addField([
			'name' => 'year',
			'label' => trans('general.year'),
			'type' => 'number',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Archivo */

		$this->crud->addField([
			'name' => 'file',
			'label' => trans('general.file'),
			'type' => 'browse',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Documentación */

		$this->crud->addField([
			'name' => 'separator_1',
			'type' => 'custom_html',
			'value' => '<h3>Región<h3><hr>',
		]);

		/* País */

		$this->crud->addField([
			'label' => trans('general.country'),
			'type' => 'select2',
			'name' => 'location_country_1_id',
			'entity' => 'country',
			'attribute' => 'name',
			'model' => 'App\Models\Location_country',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Departamento */

		$this->crud->addField([
			'label' => trans('general.department'),
			'type' => 'select2_from_ajax',
			'name' => 'location_department_1_id',
			'entity' => 'department',
			'attribute' => 'name',
			'data_source' => url('api/department'),
			'placeholder' => trans('general.select'),
			'minimum_input_length' => 0,
			'dependencies' => ['location_country_id'],
			'method' => 'POST',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Municipio */

		$this->crud->addField([
			'label' => trans('general.township'),
			'type' => 'select2_from_ajax',
			'name' => 'location_township_1_id',
			'entity' => 'township',
			'attribute' => 'name',
			'data_source' => url('api/township'),
			'placeholder' => trans('general.select'),
			'minimum_input_length' => 0,
			'dependencies' => ['location_department_id'],
			'method' => 'POST',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* País */

		$this->crud->addField([
			'label' => trans('general.country'),
			'type' => 'select2',
			'name' => 'location_country_2_id',
			'entity' => 'country',
			'attribute' => 'name',
			'model' => 'App\Models\Location_country',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Departamento */

		$this->crud->addField([
			'label' => trans('general.department'),
			'type' => 'select2_from_ajax',
			'name' => 'location_department_2_id',
			'entity' => 'department',
			'attribute' => 'name',
			'data_source' => url('api/department'),
			'placeholder' => trans('general.select'),
			'minimum_input_length' => 0,
			'dependencies' => ['location_country_id'],
			'method' => 'POST',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Municipio */

		$this->crud->addField([
			'label' => trans('general.township'),
			'type' => 'select2_from_ajax',
			'name' => 'location_township_2_id',
			'entity' => 'township',
			'attribute' => 'name',
			'data_source' => url('api/township'),
			'placeholder' => trans('general.select'),
			'minimum_input_length' => 0,
			'dependencies' => ['location_department_id'],
			'method' => 'POST',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* País */

		$this->crud->addField([
			'label' => trans('general.country'),
			'type' => 'select2',
			'name' => 'location_country_3_id',
			'entity' => 'country',
			'attribute' => 'name',
			'model' => 'App\Models\Location_country',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Departamento */

		$this->crud->addField([
			'label' => trans('general.department'),
			'type' => 'select2_from_ajax',
			'name' => 'location_department_3_id',
			'entity' => 'department',
			'attribute' => 'name',
			'data_source' => url('api/department'),
			'placeholder' => trans('general.select'),
			'minimum_input_length' => 0,
			'dependencies' => ['location_country_id'],
			'method' => 'POST',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Municipio */

		$this->crud->addField([
			'label' => trans('general.township'),
			'type' => 'select2_from_ajax',
			'name' => 'location_township_3_id',
			'entity' => 'township',
			'attribute' => 'name',
			'data_source' => url('api/township'),
			'placeholder' => trans('general.select'),
			'minimum_input_length' => 0,
			'dependencies' => ['location_department_id'],
			'method' => 'POST',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* País */

		$this->crud->addField([
			'label' => trans('general.country'),
			'type' => 'select2',
			'name' => 'location_country_4_id',
			'entity' => 'country',
			'attribute' => 'name',
			'model' => 'App\Models\Location_country',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Departamento */

		$this->crud->addField([
			'label' => trans('general.department'),
			'type' => 'select2_from_ajax',
			'name' => 'location_department_4_id',
			'entity' => 'department',
			'attribute' => 'name',
			'data_source' => url('api/department'),
			'placeholder' => trans('general.select'),
			'minimum_input_length' => 0,
			'dependencies' => ['location_country_id'],
			'method' => 'POST',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Municipio */

		$this->crud->addField([
			'label' => trans('general.township'),
			'type' => 'select2_from_ajax',
			'name' => 'location_township_4_id',
			'entity' => 'township',
			'attribute' => 'name',
			'data_source' => url('api/township'),
			'placeholder' => trans('general.select'),
			'minimum_input_length' => 0,
			'dependencies' => ['location_department_id'],
			'method' => 'POST',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* País */

		$this->crud->addField([
			'label' => trans('general.country'),
			'type' => 'select2',
			'name' => 'location_country_5_id',
			'entity' => 'country',
			'attribute' => 'name',
			'model' => 'App\Models\Location_country',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Departamento */

		$this->crud->addField([
			'label' => trans('general.department'),
			'type' => 'select2_from_ajax',
			'name' => 'location_department_5_id',
			'entity' => 'department',
			'attribute' => 'name',
			'data_source' => url('api/department'),
			'placeholder' => trans('general.select'),
			'minimum_input_length' => 0,
			'dependencies' => ['location_country_id'],
			'method' => 'POST',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Municipio */

		$this->crud->addField([
			'label' => trans('general.township'),
			'type' => 'select2_from_ajax',
			'name' => 'location_township_5_id',
			'entity' => 'township',
			'attribute' => 'name',
			'data_source' => url('api/township'),
			'placeholder' => trans('general.select'),
			'minimum_input_length' => 0,
			'dependencies' => ['location_department_id'],
			'method' => 'POST',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-4'
			],
		]);

		/* Documentación */

		$this->crud->addField([
			'name' => 'separator_2',
			'type' => 'custom_html',
			'value' => '<h3>Tags<h3><hr>',
		]);

		/* Nombre de la Organización / Empresa / Asociación / Persona que Presta los Servicios o Productos Turísticos */

		$this->crud->addField([
			'name' => 'tourism_tags',
			'label' => 'Tags de Turismo',
			'type' => 'textarea',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Nombre de la Organización / Empresa / Asociación / Persona que Presta los Servicios o Productos Turísticos */

		$this->crud->addField([
			'name' => 'topic_tags',
			'label' => 'Tags de Tema',
			'type' => 'textarea',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Autor */

		$this->crud->addField([
			'name' => 'source',
			'label' => 'Fuente',
			'type' => 'url',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Created By */

		$this->crud->addField([
			'name' => 'created_by',
			'type' => 'hidden',
			'value' => backpack_user()->id,
		], 'create');

		/* Updated By */

		$this->crud->addField([
			'name' => 'updated_by',
			'type' => 'hidden',
			'value' => backpack_user()->id,
		], 'update');

		/*
		|--------------------------------------------------------------------------
		| TABLE
		|--------------------------------------------------------------------------
		*/

		// Revisions
		$this->crud->allowAccess('revisions');
		$this->crud->with('revisionHistory');

		// Export Buttons
		$this->crud->enableExportButtons();

		/*
		|--------------------------------------------------------------------------
		| PERMISSIONS
		|--------------------------------------------------------------------------
		*/

		// Read
		if ( $user->hasPermissionTo( 'Ver Documentos' ) ) {
			$this->crud->allowAccess( 'list' );
		} else {
			$this->crud->denyAccess( 'list' );
		}

		// Create
		if ( $user->hasPermissionTo( 'Crear Documentos' ) ) {
			$this->crud->allowAccess( 'create' );
		} else {
			$this->crud->denyAccess( 'create' );
		}

		// Update
		if ( $user->hasPermissionTo( 'Editar Documentos' ) ) {
			$this->crud->allowAccess( 'update' );
		} else {
			$this->crud->denyAccess( 'update' );
		}

		// Delete
		if ( $user->hasPermissionTo( 'Borrar Documentos' ) ) {
			$this->crud->allowAccess( 'delete' );
		} else {
			$this->crud->denyAccess( 'delete' );
		}

        // add asterisk for fields that are required in DocumentRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
