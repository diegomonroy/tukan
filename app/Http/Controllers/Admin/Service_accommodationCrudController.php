<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Service_accommodationRequest as StoreRequest;
use App\Http\Requests\Service_accommodationRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class Service_accommodationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class Service_accommodationCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Service_accommodation');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/service_accommodation');
        $this->crud->setEntityNameStrings(trans('general.service_accommodation'), trans('general.service_accommodations'));

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();

		/*
		|--------------------------------------------------------------------------
		| FUNCTIONS
		|--------------------------------------------------------------------------
		*/

		$user = backpack_user();

		/*
		|--------------------------------------------------------------------------
		| CRUD
		|--------------------------------------------------------------------------
		*/

		/* ID */

		$this->crud->addColumn([
			'name' => 'id',
			'label' => trans('general.id'),
		]);

		/* Caracterización */

		$this->crud->addColumn([
			'label' => trans('general.characterization'),
			'type' => 'select',
			'name' => 'characterization_id',
			'entity' => 'characterization',
			'attribute' => 'organization_name',
			'model' => 'App\Models\Characterization',
		]);

		$this->crud->addField([
			'label' => trans('general.characterization'),
			'type' => 'select2',
			'name' => 'characterization_id',
			'entity' => 'characterization',
			'attribute' => 'organization_name',
			'model' => 'App\Models\Characterization',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Nombre */

		$this->crud->addColumn([
			'name' => 'name',
			'label' => trans('general.name'),
			'limit' => 255,
		]);

		$this->crud->addField([
			'name' => 'name',
			'label' => trans('general.name'),
			'type' => 'text',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* ¿Qué Tipo de Alojamiento Ofreces? */

		$this->crud->addColumn([
			'label' => trans('general.what_type_of_accommodation_do_you_offer'),
			'type' => 'select',
			'name' => 'type_of_accommodation_id',
			'entity' => 'type_of_accommodation',
			'attribute' => 'name',
			'model' => 'App\Models\Select_type_of_accommodation',
		]);

		$this->crud->addField([
			'label' => trans('general.what_type_of_accommodation_do_you_offer'),
			'type' => 'select2',
			'name' => 'type_of_accommodation_id',
			'entity' => 'type_of_accommodation',
			'attribute' => 'name',
			'model' => 'App\Models\Select_type_of_accommodation',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Descripción */

		$this->crud->addField([
			'name' => 'description',
			'label' => trans('general.description'),
			'type' => 'textarea',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Mínimo de Viajeros */

		$this->crud->addField([
			'name' => 'minimum_of_travelers',
			'label' => trans('general.minimum_of_travelers'),
			'type' => 'number',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-6'
			],
		]);

		/* Máximo de Viajeros */

		$this->crud->addField([
			'name' => 'maximum_of_travelers',
			'label' => trans('general.maximum_of_travelers'),
			'type' => 'number',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-6'
			],
		]);

		/* Comparte Algunas Fotos */

		$this->crud->addField([
			'name' => 'share_some_photos',
			'label' => trans('general.share_some_photos'),
			'type' => 'browse_multiple',
			'mime_types' => ['image'],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Comparte Algunos Vídeos */

		$this->crud->addField([
			'name' => 'share_some_videos',
			'label' => trans('general.share_some_videos'),
			'type' => 'browse_multiple',
			'mime_types' => ['image'],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Comparte la Ubicación en Google Maps */

		$this->crud->addField([
			'name' => 'location',
			'label' => trans('general.share_the_location_on_google_maps'),
			'type' => 'text',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Aire Acondicionado o Calefacción */

		 $this->crud->addField([
			'name' => 'air_conditioner',
			'label' => trans('general.air_conditioning_or_heating'),
			'type' => 'select_from_array',
			'options' => [
				'' => trans('general.select'),
				'yes' => trans('general.yes'),
				'no' => trans('general.no')
			],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Cuentas con Servicios Sanitarios */

		 $this->crud->addField([
			'name' => 'health_services',
			'label' => trans('general.accounts_with_health_services'),
			'type' => 'select_from_array',
			'options' => [
				'' => trans('general.select'),
				'yes' => trans('general.yes'),
				'no' => trans('general.no')
			],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Cuentas con Suministro de Permanente de Agua Potable */

		 $this->crud->addField([
			'name' => 'drinking_water',
			'label' => trans('general.accounts_with_permanent_supply_of_drinking_water'),
			'type' => 'select_from_array',
			'options' => [
				'' => trans('general.select'),
				'yes' => trans('general.yes'),
				'no' => trans('general.no')
			],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Cuentas con Suministro de Energía Eléctrica Permanente */

		 $this->crud->addField([
			'name' => 'electric_power',
			'label' => trans('general.accounts_with_permanent_power_supply'),
			'type' => 'select_from_array',
			'options' => [
				'' => trans('general.select'),
				'yes' => trans('general.yes'),
				'no' => trans('general.no')
			],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Cuentas con Telefonía (Señal o Punto de Teléfono) */

		 $this->crud->addField([
			'name' => 'telephony',
			'label' => trans('general.accounts_with_telephony_signal_or_telephone_point'),
			'type' => 'select_from_array',
			'options' => [
				'' => trans('general.select'),
				'yes' => trans('general.yes'),
				'no' => trans('general.no')
			],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Cuentas con Internet (Señal Wi-Fi o Punto de Servicio) */

		 $this->crud->addField([
			'name' => 'internet',
			'label' => trans('general.accounts_with_internet_wi_fi_signal_or_point_of_service'),
			'type' => 'select_from_array',
			'options' => [
				'' => trans('general.select'),
				'yes' => trans('general.yes'),
				'no' => trans('general.no')
			],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Cuentas con Condiciones de Alojamiento para */

		$this->crud->addField([
			'label' => trans('general.you_have_accommodation_conditions_for'),
			'type' => 'select2',
			'name' => 'accommodation_for_id',
			'entity' => 'accommodation_for',
			'attribute' => 'name',
			'model' => 'App\Models\Select_accommodation_for',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Para Cuántas Personas en Total Tienes Disponibilidad */

		$this->crud->addField([
			'name' => 'people_in_total',
			'label' => trans('general.for_how_many_people_in_total_have_availability'),
			'type' => 'number',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Cuántas en Acomodación Sencilla */

		$this->crud->addField([
			'name' => 'simple_accommodation',
			'label' => trans('general.how_many_in_simple_accommodation'),
			'type' => 'number',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-6'
			],
		]);

		/* Cuántas en Acomodación Doble */

		$this->crud->addField([
			'name' => 'double_accommodation',
			'label' => trans('general.how_many_in_double_accommodation'),
			'type' => 'number',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-6'
			],
		]);

		/* Cuántas en Acomodación Compartida */

		$this->crud->addField([
			'name' => 'shared_accommodation',
			'label' => trans('general.how_many_in_shared_accommodation'),
			'type' => 'number',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-6'
			],
		]);

		/* Cuántas en Otros Tipos de Acomodación */

		$this->crud->addField([
			'name' => 'other_types_of_accommodation',
			'label' => trans('general.how_many_other_types_of_accommodation'),
			'type' => 'number',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-6'
			],
		]);

		/* Created By */

		$this->crud->addField([
			'name' => 'created_by',
			'type' => 'hidden',
			'value' => backpack_user()->id,
		], 'create');

		/* Updated By */

		$this->crud->addField([
			'name' => 'updated_by',
			'type' => 'hidden',
			'value' => backpack_user()->id,
		], 'update');

		/*
		|--------------------------------------------------------------------------
		| TABLE
		|--------------------------------------------------------------------------
		*/

		// Revisions
		$this->crud->allowAccess('revisions');
		$this->crud->with('revisionHistory');

		// Export Buttons
		$this->crud->enableExportButtons();

		/*
		|--------------------------------------------------------------------------
		| PERMISSIONS
		|--------------------------------------------------------------------------
		*/

		// Read
		if ( $user->hasPermissionTo( 'Ver Alojamientos' ) ) {
			$this->crud->allowAccess( 'list' );
		} else {
			$this->crud->denyAccess( 'list' );
		}

		// Create
		if ( $user->hasPermissionTo( 'Crear Alojamientos' ) ) {
			$this->crud->allowAccess( 'create' );
		} else {
			$this->crud->denyAccess( 'create' );
		}

		// Update
		if ( $user->hasPermissionTo( 'Editar Alojamientos' ) ) {
			$this->crud->allowAccess( 'update' );
		} else {
			$this->crud->denyAccess( 'update' );
		}

		// Delete
		if ( $user->hasPermissionTo( 'Borrar Alojamientos' ) ) {
			$this->crud->allowAccess( 'delete' );
		} else {
			$this->crud->denyAccess( 'delete' );
		}

        // add asterisk for fields that are required in Service_accommodationRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
