<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Service_transportRequest as StoreRequest;
use App\Http\Requests\Service_transportRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class Service_transportCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class Service_transportCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Service_transport');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/service_transport');
        $this->crud->setEntityNameStrings(trans('general.service_transport'), trans('general.service_transports'));

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();

		/*
		|--------------------------------------------------------------------------
		| FUNCTIONS
		|--------------------------------------------------------------------------
		*/

		$user = backpack_user();

		/*
		|--------------------------------------------------------------------------
		| CRUD
		|--------------------------------------------------------------------------
		*/

		/* ID */

		$this->crud->addColumn([
			'name' => 'id',
			'label' => trans('general.id'),
		]);

		/* Caracterización */

		$this->crud->addColumn([
			'label' => trans('general.characterization'),
			'type' => 'select',
			'name' => 'characterization_id',
			'entity' => 'characterization',
			'attribute' => 'organization_name',
			'model' => 'App\Models\Characterization',
		]);

		$this->crud->addField([
			'label' => trans('general.characterization'),
			'type' => 'select2',
			'name' => 'characterization_id',
			'entity' => 'characterization',
			'attribute' => 'organization_name',
			'model' => 'App\Models\Characterization',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Nombre */

		$this->crud->addColumn([
			'name' => 'name',
			'label' => trans('general.name'),
			'limit' => 255,
		]);

		$this->crud->addField([
			'name' => 'name',
			'label' => trans('general.name'),
			'type' => 'text',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* ¿Qué Tipo de Servicios de Transporte Presta? */

		$this->crud->addColumn([
			'label' => trans('general.what_kind_of_transportation_services_do_you_provide'),
			'type' => 'select',
			'name' => 'type_of_transport_id',
			'entity' => 'type_of_transport',
			'attribute' => 'name',
			'model' => 'App\Models\Select_type_of_transport',
		]);

		$this->crud->addField([
			'label' => trans('general.what_kind_of_transportation_services_do_you_provide'),
			'type' => 'select2',
			'name' => 'type_of_transport_id',
			'entity' => 'type_of_transport',
			'attribute' => 'name',
			'model' => 'App\Models\Select_type_of_transport',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Descripción */

		$this->crud->addField([
			'name' => 'description',
			'label' => trans('general.description'),
			'type' => 'textarea',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Especificaciones */

		$this->crud->addField([
			'name' => 'specifications',
			'label' => trans('general.specifications'),
			'type' => 'textarea',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Mínimo de Viajeros */

		$this->crud->addField([
			'name' => 'minimum_of_travelers',
			'label' => trans('general.minimum_of_travelers'),
			'type' => 'number',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-6'
			],
		]);

		/* Máximo de Viajeros */

		$this->crud->addField([
			'name' => 'maximum_of_travelers',
			'label' => trans('general.maximum_of_travelers'),
			'type' => 'number',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-6'
			],
		]);

		/* Comparte Algunas Fotos */

		$this->crud->addField([
			'name' => 'share_some_photos',
			'label' => trans('general.share_some_photos'),
			'type' => 'browse_multiple',
			'mime_types' => ['image'],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Comparte Algunos Vídeos */

		$this->crud->addField([
			'name' => 'share_some_videos',
			'label' => trans('general.share_some_videos'),
			'type' => 'browse_multiple',
			'mime_types' => ['image'],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Comparte la Ubicación en Google Maps */

		$this->crud->addField([
			'name' => 'location',
			'label' => trans('general.share_the_location_on_google_maps'),
			'type' => 'text',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* ¿Cuentas con Dispositivos de Seguridad para el Viajero? */

		 $this->crud->addField([
			'name' => 'security_devices',
			'label' => trans('general.do_you_have_security_devices_for_the_traveler'),
			'type' => 'select_from_array',
			'options' => [
				'' => trans('general.select'),
				'yes' => trans('general.yes'),
				'no' => trans('general.no')
			],
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* ¿Cuentas con la Documentación Pertinente? */

		$this->crud->addField([
			'label' => trans('general.do_you_have_the_relevant_documentation'),
			'type' => 'select2',
			'name' => 'relevant_documentation_id',
			'entity' => 'relevant_documentation',
			'attribute' => 'name',
			'model' => 'App\Models\Select_relevant_documentation',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* ¿Cuéntas con Seguro de Accidentes? */

		$this->crud->addField([
			'label' => trans('general.how_many_with_accident_insurance'),
			'type' => 'select2',
			'name' => 'accident_insurance_id',
			'entity' => 'accident_insurance',
			'attribute' => 'name',
			'model' => 'App\Models\Select_accident_insurance',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Created By */

		$this->crud->addField([
			'name' => 'created_by',
			'type' => 'hidden',
			'value' => backpack_user()->id,
		], 'create');

		/* Updated By */

		$this->crud->addField([
			'name' => 'updated_by',
			'type' => 'hidden',
			'value' => backpack_user()->id,
		], 'update');

		/*
		|--------------------------------------------------------------------------
		| TABLE
		|--------------------------------------------------------------------------
		*/

		// Revisions
		$this->crud->allowAccess('revisions');
		$this->crud->with('revisionHistory');

		// Export Buttons
		$this->crud->enableExportButtons();

		/*
		|--------------------------------------------------------------------------
		| PERMISSIONS
		|--------------------------------------------------------------------------
		*/

		// Read
		if ( $user->hasPermissionTo( 'Ver Transportes' ) ) {
			$this->crud->allowAccess( 'list' );
		} else {
			$this->crud->denyAccess( 'list' );
		}

		// Create
		if ( $user->hasPermissionTo( 'Crear Transportes' ) ) {
			$this->crud->allowAccess( 'create' );
		} else {
			$this->crud->denyAccess( 'create' );
		}

		// Update
		if ( $user->hasPermissionTo( 'Editar Transportes' ) ) {
			$this->crud->allowAccess( 'update' );
		} else {
			$this->crud->denyAccess( 'update' );
		}

		// Delete
		if ( $user->hasPermissionTo( 'Borrar Transportes' ) ) {
			$this->crud->allowAccess( 'delete' );
		} else {
			$this->crud->denyAccess( 'delete' );
		}

        // add asterisk for fields that are required in Service_transportRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
