<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Service_accommodationCollection;
use App\Models\Service_accommodation;

class Service_accommodationController extends Controller
{

	/*
	 * Read
	 */
	public function index() {
		return new Service_accommodationCollection( Service_accommodation::all() );
	}

	/*
	 * Create
	 */
	public function store( Request $request ) {
		$item = new Service_accommodation([
			'characterization_id' => 1,
			'name' => $request->get('name'),
			'type_of_accommodation_id' => $request->get('type_of_accommodation_id'),
			'description' => $request->get('description'),
			'minimum_of_travelers' => $request->get('minimum_of_travelers'),
			'maximum_of_travelers' => $request->get('maximum_of_travelers'),
			'share_some_photos' => $request->get('share_some_photos'),
			'share_some_videos' => $request->get('share_some_videos'),
			'location' => $request->get('location'),
			'air_conditioner' => $request->get('air_conditioner'),
			'health_services' => $request->get('health_services'),
			'drinking_water' => $request->get('drinking_water'),
			'electric_power' => $request->get('electric_power'),
			'telephony' => $request->get('telephony'),
			'internet' => $request->get('internet'),
			'accommodation_for_id' => $request->get('accommodation_for_id'),
			'people_in_total' => $request->get('people_in_total'),
			'simple_accommodation' => $request->get('simple_accommodation'),
			'double_accommodation' => $request->get('double_accommodation'),
			'shared_accommodation' => $request->get('shared_accommodation'),
			'other_types_of_accommodation' => $request->get('other_types_of_accommodation'),
		]);
		$item->save();
		return response()->json('Successfully Added');
	}

	/*
	 * Edit
	 */
	public function edit( $id ) {
		$item = Service_accommodation::find( $id );
		return response()->json($item);
	}

	/*
	 * Update
	 */
	public function update( $id, Request $request ) {
		$item = Service_accommodation::find( $id );
		$item->update($request->all());
		return response()->json('Successfully Updated');
	}

	/*
	 * Delete
	 */
	public function delete( $id ) {
		$item = Service_accommodation::find( $id );
		$item->delete();
		return response()->json('Successfully Deleted');
	}

}
