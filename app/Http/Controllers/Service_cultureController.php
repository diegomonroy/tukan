<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Service_cultureCollection;
use App\Models\Service_culture;

class Service_cultureController extends Controller
{

	/*
	 * Read
	 */
	public function index() {
		return new Service_cultureCollection( Service_culture::all() );
	}

	/*
	 * Create
	 */
	public function store( Request $request ) {
		$item = new Service_culture([
			'characterization_id' => 1,
			'name' => $request->get('name'),
			'type_of_culture' => $request->get('type_of_culture'),
			'description' => $request->get('description'),
			'specifications' => $request->get('specifications'),
			'share_some_photos' => $request->get('share_some_photos'),
			'share_some_videos' => $request->get('share_some_videos'),
			'location' => $request->get('location'),
		]);
		$item->save();
		return response()->json('Successfully Added');
	}

	/*
	 * Edit
	 */
	public function edit( $id ) {
		$item = Service_culture::find( $id );
		return response()->json($item);
	}

	/*
	 * Update
	 */
	public function update( $id, Request $request ) {
		$item = Service_culture::find( $id );
		$item->update($request->all());
		return response()->json('Successfully Updated');
	}

	/*
	 * Delete
	 */
	public function delete( $id ) {
		$item = Service_culture::find( $id );
		$item->delete();
		return response()->json('Successfully Deleted');
	}

}
