<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\CharacterizationCollection;
use App\Models\Characterization;

class CharacterizationController extends Controller
{

	/*
	 * Read
	 */
	public function index() {
		return new CharacterizationCollection( Characterization::all() );
	}

	/*
	 * Create
	 */
	public function store( Request $request ) {
		$item = new Characterization([
			'user_id' => $request->get('user_id'),
			'email' => $request->get('email'),
			'first_name' => $request->get('first_name'),
			'last_name' => $request->get('last_name'),
			'phone' => $request->get('phone'),
		]);
		$item->save();
		return response()->json('Successfully Added');
	}

	/*
	 * Edit
	 */
	public function edit( $id ) {
		$item = Characterization::find( $id );
		return response()->json($item);
	}

	/*
	 * Delete
	 */
	public function delete( $id ) {
		$item = Characterization::find( $id );
		$item->delete();
		return response()->json('Successfully Deleted');
	}

}
