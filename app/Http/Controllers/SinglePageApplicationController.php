<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SinglePageApplicationController extends Controller
{

	/*
	 * Show Index page
	 */
	public function index() {
		return view('layouts.app');
	}

}
