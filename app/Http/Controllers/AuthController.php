<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\ResetsPasswords;

use Hash;
use Illuminate\Auth\Events\PasswordReset;

class AuthController extends Controller
{

	/*
	 * Send Register
	 */
	public function register( Request $request ) {
		$v = Validator::make($request->all(), [
			'name' => 'required|min:3',
			'email' => 'required|email|unique:users',
			'password' => 'required|min:3|confirmed',
		]);
		if ( $v->fails() ) {
			return response()->json([
				'status' => 'error',
				'errors' => $v->errors()
			], 422);
		}
		$user = new User;
		$user->name = $request->name;
		$user->email = $request->email;
		$user->password = bcrypt($request->password);
		$user->save();
		return response()->json([
			'status' => 'success'
		], 200);
	}

	/*
	 * Send Login
	 */
	public function login( Request $request ) {
		$credentials = $request->only('email', 'password');
		if ( $token = $this->guard()->attempt($credentials) ) {
			return response()->json([
				'status' => 'success'
			], 200)->header('Authorization', $token);
		}
		return response()->json([
			'error' => 'login_error'
		], 401);
	}

	/*
	 * Do Refresh
	 */
	public function refresh() {
		if ( $token = $this->guard()->refresh() ) {
			return response()->json([
				'status' => 'successs'
			], 200)->header('Authorization', $token);
		}
		return response()->json([
			'error' => 'refresh_token_error'
		], 401);
	}

	/*
	 * Show User page
	 */
	public function user( Request $request ) {
		$user = User::find( Auth::user()->id );
		return response()->json([
			'status' => 'success',
			'data' => $user
		]);
	}

	/*
	 * Do Logout
	 */
	public function logout() {
		$this->guard()->logout();
		return response()->json([
			'status' => 'success',
			'msg' => 'Logged Out Successfully.'
		], 200);
	}

	/*
	 * Return Guard
	 */
	private function guard() {
		return Auth::guard();
	}

	/*
	 * Send Password Reset Link
	 */
	public function sendPasswordResetLink( Request $request ) {
		return $this->sendResetLinkEmail($request);
	}

	/**
	 * Get the response for a successful password reset link.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  string  $response
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
	 */
	protected function sendResetLinkResponse( Request $request, $response ) {
		return response()->json([
			'message' => 'Password reset email sent.',
			'data' => $response
		]);
	}

	/**
	 * Get the response for a failed password reset link.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  string  $response
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
	 */
	protected function sendResetLinkFailedResponse( Request $request, $response ) {
		return response()->json([
			'message' => 'Email could not be sent to this email address.'
		]);
	}

	/*
	 * Handle Reset Password
	 */
	public function callResetPassword( Request $request ) {
		return $this->reset($request);
	}

	/**
	 * Reset the given user's password.
	 *
	 * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
	 * @param  string  $password
	 * @return void
	 */
	protected function resetPassword( $user, $password ) {
		$user->password = Hash::make($password);
		$user->save();
		event(new PasswordReset($user));
	}

	/**
	 * Get the response for a successful password reset.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  string  $response
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
	 */
	protected function sendResetResponse( Request $request, $response ) {
		return response()->json([
			'message' => 'Password reset successfully.'
		]);
	}

	/**
	 * Get the response for a failed password reset.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  string  $response
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
	 */
	protected function sendResetFailedResponse( Request $request, $response ) {
		return response()->json([
			'message' => 'Failed, Invalid Token.'
		]);
	}

}
