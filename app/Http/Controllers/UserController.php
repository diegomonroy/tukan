<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

	/*
	 * Show Index page
	 */
	public function index() {
		$users = User::all();
		return response()->json([
			'status' => 'success',
			'users' => $users->toArray()
		], 200);
	}

	/*
	 * Show Show page
	 */
	public function show( Request $request, $id ) {
		$user = User::find( $id );
		return response()->json([
			'status' => 'success',
			'user' => $user->toArray()
		], 200);
	}

}
